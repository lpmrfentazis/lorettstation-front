// i18next-parser.config.ts
import { UserConfig } from 'i18next-parser'

const obj: UserConfig = {
  input: ['src/**/*.{js,jsx,ts,tsx}'],
  createOldCatalogs: true,
  defaultNamespace: 'translation',
  output: 'public/locales/$LOCALE/$NAMESPACE.json',
  locales: ['ru', 'en', 'it'],
  failOnWarnings: false,
  keepRemoved: [/dynamic/],
}

export default obj
