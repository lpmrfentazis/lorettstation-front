import { GLOBAL_COLORS } from '@/config/constants'

import { useThemedColors } from '@/hooks/colorScheme'

export const GlobalContainer = ({
  children,
}: {
  children: React.ReactNode
}) => {
  useThemedColors(GLOBAL_COLORS)

  return <>{children}</>
}
