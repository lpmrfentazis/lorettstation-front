import { Box } from '@mui/joy'

export const Wrapper = ({ children }: { children: React.ReactNode }) => {
  return (
    <Box
      sx={{
        px: '20px',
      }}
    >
      {children}
    </Box>
  )
}
