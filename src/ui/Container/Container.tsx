import { Box } from '@mui/joy'

export const Container = ({ children }: { children: React.ReactNode }) => {
  return (
    <Box
      sx={{
        maxWidth: 1000,
        m: '0 auto',
        px: '20px',
      }}
    >
      {children}
    </Box>
  )
}
