import type { Map } from 'leaflet'
import { createWithEqualityFn } from 'zustand/traditional'

import { MAP_TYPE } from '@/config/constants'

import { LatLng } from '@/types/main'

type MapType = keyof typeof MAP_TYPE

type MapStore = {
  mapMode: MapType
  changeMapMode: (mode: MapType) => void

  stationPos: LatLng | null
  setStationPos: (arg: LatLng) => void

  extraPos: LatLng | null
  setExtraPos: (arg: LatLng | null) => void

  activeSalist: string[]
  toggleSatlistItem: (item: string) => void
  setActiveSatlist: (list: string[]) => void

  shownSatellite: string | null
  setShownSatellite: (arg: string | null) => void

  mapInstance: Map | null
  setMapInstance: (arg: Map) => void
}

export const useMapStore = createWithEqualityFn<MapStore>()(set => ({
  mapMode: 'schema',
  changeMapMode: mode => set({ mapMode: mode }),

  stationName: '',
  stationPos: null,
  setStationPos: value => set({ stationPos: value }),

  extraPos: null,
  setExtraPos: value => {
    if (value === null) {
      set({ extraPos: value })
    } else {
      const newExtraPos = {
        lat: parseFloat(value.lat.toFixed(6)),
        lng: parseFloat(value.lng.toFixed(6)),
      }
      set({ extraPos: newExtraPos })
    }
  },

  activeSalist: [],
  toggleSatlistItem: item => {
    set(state => {
      const curList = state.activeSalist
      const idx = curList.indexOf(item)
      let newList
      if (idx !== -1) {
        const newSelection = [...curList]
        newSelection.splice(idx, 1)
        newList = { activeSalist: [...newSelection] }
      } else {
        newList = { activeSalist: [...curList, item] }
      }
      localStorage.setItem(
        'activeSatlist',
        JSON.stringify(newList.activeSalist),
      )
      return newList
    })
  },
  setActiveSatlist: list => {
    set({ activeSalist: [...list] })
    localStorage.setItem('activeSatlist', JSON.stringify(list))
  },

  shownSatellite: null,
  setShownSatellite: value => set({ shownSatellite: value }),

  mapInstance: null,
  setMapInstance: value => set({ mapInstance: value }),
}))
