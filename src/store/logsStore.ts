import { createWithEqualityFn } from 'zustand/traditional'

import { LIMIT_INCREMENT } from '@/config/constants'

import { TLogSchema } from '@/types/logs'

type LogsStore = {
  logs: TLogSchema[]
  offset: number
  limit: number
  changeLimit: (limit: number) => void
  incrementLimit: (value?: number) => void
  setLogs: (logs: TLogSchema[]) => void
  addLogs: (logs: TLogSchema[]) => boolean
}

export const useLogsStore = createWithEqualityFn<LogsStore>()(set => ({
  offset: 0,
  limit: 20,
  logs: [],
  changeLimit: (limit: number) => {
    set(state => ({ ...state, limit }))
  },

  incrementLimit: value =>
    set(state => ({
      ...state,
      limit: state.limit + (value ? value : LIMIT_INCREMENT),
    })),

  setLogs: logs => set(state => ({ ...state, logs })),

  addLogs: logs => {
    let newLogs: TLogSchema[] = []
    const { logs: curLogs, limit } = useLogsStore.getState()

    if (
      logs[logs.length - 1] &&
      curLogs.some(log => log.timeMoment === logs[logs.length - 1].timeMoment)
    ) {
      const firstLog = logs
        .reverse()
        .find(
          log => !curLogs.some(curLog => curLog.timeMoment === log.timeMoment),
        )

      if (firstLog) {
        logs.reverse()
        logs.splice(logs.indexOf(firstLog) + 1)

        newLogs = [...logs, ...curLogs].slice(0, limit)
      } else {
        newLogs = [...curLogs]
      }
    } else {
      return true
    }

    set(state => {
      return {
        ...state,
        logs: newLogs,
      }
    })

    return false
  },
}))
