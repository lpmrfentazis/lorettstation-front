import { createWithEqualityFn } from 'zustand/traditional'

import { envConfig } from '@/config/constants'

type ScheduleStore = {
  logsLimit: number
  scheduleLimit: number
  setLimits: (obj: { logsLimit?: number; scheduleLimit?: number }) => void

  lastUpdate: Date | null
  setLastUpdate: (date: Date) => boolean
}

export const useScheduleStore = createWithEqualityFn<ScheduleStore>()(set => ({
  logsLimit: parseInt(
    localStorage.getItem('logsLimit') || `${envConfig.defaultLogsLimit}`,
  ),
  scheduleLimit: parseInt(
    localStorage.getItem('scheduleLimit') ||
      `${envConfig.defaultScheduleLimit}`,
  ),

  setLimits: ({ logsLimit, scheduleLimit }) => {
    if (logsLimit) {
      set(state => ({ ...state, logsLimit }))
      localStorage.setItem('logsLimit', logsLimit.toString())
    }
    if (scheduleLimit) {
      set(state => ({ ...state, scheduleLimit }))
      localStorage.setItem('scheduleLimit', scheduleLimit.toString())
    }
  },

  lastUpdate: null,
  setLastUpdate: date => {
    const curDate = useScheduleStore.getState().lastUpdate

    if (curDate && date > curDate) {
      set(state => ({ ...state, lastUpdate: date }))
      return true
    }

    if (!curDate) {
      set(state => ({ ...state, lastUpdate: date }))
    }

    return false
  },
}))
