import { createWithEqualityFn } from 'zustand/traditional'

type ControlStore = {
  userRole: number | null
  setUserRole: (arg: number) => void

  hasControlPanel: boolean
  setHasControlPanel: (value: boolean) => void
}

export const useControlStore = createWithEqualityFn<ControlStore>()(set => ({
  userRole: null,
  setUserRole: value => set({ userRole: value }),

  hasControlPanel: false,
  setHasControlPanel: value => {
    set(state => ({ ...state, hasControlPanel: value }))
  },
}))
