import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query'
import { AxiosError } from 'axios'
import { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import { toast } from 'sonner'

import loginService from '@/services/loginService'

import { useControlStore } from '@/store/controlStore'

import { saveAccessToken } from '@/utils/login'

import { TLoginRespSchema, TLoginSchema } from '@/types/login'

export const useLogin = () => {
  const { t } = useTranslation()
  const navigate = useNavigate()
  const client = useQueryClient()

  return useMutation({
    mutationKey: ['login'],
    mutationFn: (data: TLoginSchema) => loginService.login(data),

    onSuccess: (data: TLoginRespSchema) => {
      saveAccessToken(data.token)
      client.invalidateQueries({ queryKey: ['user'] })
      navigate(-1)
    },

    onError: (data: AxiosError<{ detail: string }>) => {
      if (data.response?.data.detail) {
        toast.error(t('toast.authFail'))
      }
    },
  })
}

export const useUser = () => {
  const setUserRole = useControlStore(state => state.setUserRole)

  const {
    data: userData,
    isFetching: isUserLoading,
    isError: isUserError,
  } = useQuery({
    queryKey: ['user'],
    queryFn: () => loginService.getUser(),
    retry: false,
  })

  useEffect(() => {
    if (!userData) return
    setUserRole(userData.roleID)
  }, [userData])

  return { userData, isUserLoading, isUserError }
}
