import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query'
import { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { toast } from 'sonner'

import orbitalsService from '@/services/orbitalsService'
import recordsService from '@/services/recordsService'

import { envConfig } from '@/config/constants'

import { useScheduleStore } from '@/store/scheduleStore'

import { PlannedFullReq, TPlannedFlySchema } from '@/types/schedule'

export const usePassedSchedule = () => {
  const limit = useScheduleStore(state => state.logsLimit)

  const {
    data: passedData,
    isLoading: isPassedLoading,
    refetch,
  } = useQuery({
    queryKey: ['schedule', 'logs'],
    queryFn: () => recordsService.getPassedSchedule(limit),
  })

  useEffect(() => {
    refetch()
  }, [limit, refetch])

  return { passedData, isPassedLoading }
}

export const usePassedScheduleFull = (passID?: string) => {
  return useQuery({
    queryKey: ['schedule_full', 'logs', passID],
    queryFn: () => recordsService.getPassedScheduleFull(passID ?? ''),
    enabled: !!passID,
  })
}

export const usePlannedSchedule = () => {
  const limit = useScheduleStore(state => state.scheduleLimit)

  const {
    data: plannedData,
    isLoading: isPlannedLoading,
    refetch,
  } = useQuery({
    queryKey: ['schedule', 'planned'],
    queryFn: () => orbitalsService.getPlannedSchedule(limit),
  })

  useEffect(() => {
    refetch()
  }, [limit, refetch])

  return { plannedData, isPlannedLoading }
}

export const useUpdateSchedule = () => {
  const { t } = useTranslation()

  const { mutate: updateSchedule } = useMutation({
    mutationKey: ['update_schedule'],
    mutationFn: (data: TPlannedFlySchema) =>
      orbitalsService.putPlannedSchedule(data),
    onSuccess: () => {
      toast.info(t('toast.success'))
    },
  })

  return { updateSchedule }
}

export const usePlannedScheduleFull = (req?: PlannedFullReq | null) => {
  const reqParams = new URLSearchParams(req || '')

  return useQuery({
    queryKey: ['schedule_full', 'planned', reqParams.toString()],
    queryFn: () => orbitalsService.getPlannedScheduleFull(reqParams.toString()),
    enabled: !!req?.satellite && !!req?.timeEnd,
  })
}

export const useLastUpdateSchedule = () => {
  const { setLastUpdate } = useScheduleStore()
  const client = useQueryClient()

  const { data } = useQuery({
    queryKey: ['last_update_schedule'],
    queryFn: () => orbitalsService.getLastUpdateSchedule(),
    refetchInterval: envConfig.scheduleUpdateTimeout,
  })

  useEffect(() => {
    if (!data) return
    const timeStamp = new Date(data)
    const shouldUpdate = setLastUpdate(timeStamp)

    if (shouldUpdate) {
      client.invalidateQueries({
        queryKey: ['schedule'],
      })
    }
  }, [client, data, setLastUpdate])
}
