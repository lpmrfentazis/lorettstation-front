import { useQuery } from '@tanstack/react-query'

import recordsService from '@/services/recordsService'

import { envConfig } from '@/config/constants'

export const useExplorer = () => {
  return useQuery({
    queryKey: ['explorer'],
    queryFn: () => fetch(envConfig.explorerUrl),
    retry: false,
  })
}

export const useExplorerPath = (passId?: string) => {
  return useQuery({
    queryKey: ['explorer_path'],
    queryFn: () => recordsService.getExplorerPath(passId ?? ''),
    enabled: !!passId,
  })
}
