import { useMutation, useQuery } from '@tanstack/react-query'
import { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { toast } from 'sonner'

import rotatorService from '@/services/rotatorService'

import { envConfig } from '@/config/constants'

import { useControlStore } from '@/store/controlStore'

import { TControlSchema } from '@/types/control'

export const useControlToStore = () => {
  const setHasControlPanel = useControlStore(state => state.setHasControlPanel)

  const { data: controlData } = useQuery({
    queryKey: ['control'],
    queryFn: () => rotatorService.getControl(),
  })

  useEffect(() => {
    if (!controlData) return
    if (controlData.azimuth === null && controlData.elevation === null) {
      setHasControlPanel(false)
    } else {
      setHasControlPanel(true)
    }
  }, [controlData, setHasControlPanel])

  return { controlData }
}

export const useControl = () => {
  const { data: controlData } = useQuery({
    queryKey: ['control'],
    queryFn: () => rotatorService.getControl(),
    refetchInterval: envConfig.controlUpdateTimeout,
  })

  return { controlData }
}

export const useMovementsRelative = () => {
  const { t } = useTranslation()

  const { mutate: relativeMove, isPending: isRelativePending } = useMutation({
    mutationKey: ['movement', 'relative'],
    mutationFn: (data: TControlSchema) => rotatorService.relativeMove(data),
    onSuccess: () => {
      toast.success(t('toast.success'))
    },
    onError: () => {
      toast.error(t('toast.error'))
    },
  })

  return { relativeMove, isRelativePending }
}

export const useMovementsAbsolute = () => {
  const { t } = useTranslation()

  const { mutate: absoluteMove, isPending: isAbsolutePending } = useMutation({
    mutationKey: ['movement', 'absolute'],
    mutationFn: (data: TControlSchema) => rotatorService.absoluteMove(data),
    onSuccess: () => {
      toast.success(t('toast.success'))
    },
    onError: () => {
      toast.error(t('toast.error'))
    },
  })

  return { absoluteMove, isAbsolutePending }
}

export const useMovementsStop = () => {
  const { t } = useTranslation()

  const { mutate: stopMove, isPending: isStopPending } = useMutation({
    mutationKey: ['movement', 'stop'],
    mutationFn: () => rotatorService.stop(),
    onSuccess: () => {
      toast.success(t('toast.success'))
    },
    onError: () => {
      toast.error(t('toast.error'))
    },
  })

  return { stopMove, isStopPending }
}

export const useMovementsCalibrate = () => {
  const { t } = useTranslation()

  const { mutate: calibrateMove, isPending: isCalibratePending } = useMutation({
    mutationKey: ['movement', 'calibrate'],
    mutationFn: () => rotatorService.calibrate(),
    onSuccess: () => {
      toast.success(t('toast.success'))
    },
    onError: () => {
      toast.error(t('toast.error'))
    },
  })

  return { calibrateMove, isCalibratePending }
}

export const useMovementsStartPosition = () => {
  const { t } = useTranslation()

  const { mutate: startPosition, isPending: isStartPositionPending } =
    useMutation({
      mutationKey: ['movement', 'start_position'],
      mutationFn: () => rotatorService.startPosition(),
      onSuccess: () => {
        toast.success(t('toast.success'))
      },
      onError: () => {
        toast.error(t('toast.error'))
      },
    })

  return { startPosition, isStartPositionPending }
}

export const useMovementsSaveLandmark = () => {
  const { t } = useTranslation()

  const { mutate: saveLandmark, isPending: isSaveLandmarkPending } =
    useMutation({
      mutationKey: ['movement', 'save_landmark'],
      mutationFn: (data: { value: number }) =>
        rotatorService.saveLandmark(data),
      onSuccess: () => {
        toast.success(t('toast.success'))
      },
      onError: () => {
        toast.error(t('toast.error'))
      },
    })

  return { saveLandmark, isSaveLandmarkPending }
}
