import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query'
import { useTranslation } from 'react-i18next'
import { toast } from 'sonner'

import orbitalsService from '@/services/orbitalsService'
import telemetryService from '@/services/telemetryService'

import { envConfig } from '@/config/constants'

import { PlannedFullReq } from '@/types/schedule'
import { TSettingsSchema } from '@/types/settings'

export const useSettings = () => {
  const { data: settingsData } = useQuery({
    queryKey: ['settings'],
    queryFn: () => telemetryService.getSettings(),
    refetchInterval: envConfig.settignsUpdateTimeout,
  })

  return { settingsData }
}

export const useUpdateSettings = () => {
  const client = useQueryClient()
  const { t } = useTranslation()

  const { mutate: updateSettings, isPending } = useMutation({
    mutationKey: ['update_settings'],
    mutationFn: (data: TSettingsSchema) => telemetryService.putSettings(data),
    onSuccess: () => {
      client.invalidateQueries({ queryKey: ['settings'] })
      toast.success(t('toast.success'))
    },
  })

  return { updateSettings, isPending }
}

export const useSatlistFull = () => {
  const { data: fullData, isLoading } = useQuery({
    queryKey: ['satlist_full'],
    queryFn: () => telemetryService.getFullSatlist(),
  })

  return { fullData, isLoading }
}

export const useSatlistActive = () => {
  const { data: acitveData, isLoading } = useQuery({
    queryKey: ['satlist_active'],
    queryFn: () => telemetryService.getActiveSatlist(),
  })

  return { acitveData, isLoading }
}

export const useUpdateSatlist = () => {
  const { mutate: updateSatlist } = useMutation({
    mutationKey: ['update_satlist'],
    mutationFn: (data: string[]) => telemetryService.putActiveSatlist(data),
  })
  return { updateSatlist }
}

export const useTrajectory = (req?: PlannedFullReq) => {
  const reqParams = new URLSearchParams(req || '')

  const { data: trjectoryData } = useQuery({
    queryKey: ['trajectory', reqParams.toString()],
    queryFn: () => orbitalsService.getPlannedTrajectory(reqParams.toString()),
    enabled: !!req?.satellite && !!req?.timeEnd,
  })

  return { trjectoryData }
}
