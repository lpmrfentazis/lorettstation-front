import { useQuery, useQueryClient } from '@tanstack/react-query'
import { useEffect } from 'react'

import telemetryService from '@/services/telemetryService'

import { envConfig } from '@/config/constants'

import { useLogsStore } from '@/store/logsStore'

const SMALL_LIMIT = 20

export const useLogs = () => {
  const { offset, limit, setLogs } = useLogsStore(
    ({ offset, limit, setLogs }) => ({
      offset,
      limit,
      setLogs,
    }),
  )

  const { data, refetch, isLoading, isFetching } = useQuery({
    queryKey: ['logs'],
    queryFn: () => telemetryService.getLogs({ offset: offset, limit: limit }),
    retry: false,
  })

  useEffect(() => {
    refetch()
  }, [limit, refetch])

  useEffect(() => {
    if (!data) return
    setLogs(data)
  }, [data, setLogs])

  useMiniLogs()

  return { isLoading, isFetching }
}

/**
 * Хук для регулярного обновления логов
 */
export const useMiniLogs = () => {
  const client = useQueryClient()
  const { addLogs, incrementLimit } = useLogsStore(
    ({ offset, limit, addLogs, incrementLimit }) => ({
      offset,
      limit,
      addLogs,
      incrementLimit,
    }),
  )

  const { data } = useQuery({
    queryKey: ['mini_logs'],
    queryFn: () => telemetryService.getLogs({ limit: SMALL_LIMIT }),
    retry: false,
    refetchInterval: envConfig.logsUpdateTimeout,
  })

  useEffect(() => {
    if (!data) return

    const shouldInvalidate = addLogs(data)
    if (shouldInvalidate) {
      incrementLimit(SMALL_LIMIT)
      client.invalidateQueries({ queryKey: ['logs'] })
    }
  }, [addLogs, client, data, incrementLimit])
}
