import { useQuery } from '@tanstack/react-query'

import telemetryService from '@/services/telemetryService'

import { envConfig } from '@/config/constants'

export const useTelemetry = () => {
  return useQuery({
    queryKey: ['telemetry'],
    queryFn: () => telemetryService.getSummary(),
    refetchInterval: envConfig.telemetryUpdateTimeout,
  })
}
