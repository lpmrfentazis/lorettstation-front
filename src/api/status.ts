import { useQuery } from '@tanstack/react-query'

import telemetryService from '@/services/telemetryService'

export const useStatus = () => {
  const { data: statusData } = useQuery({
    queryKey: ['status'],
    queryFn: () => telemetryService.getStatus(),
    refetchInterval: 5000,
  })

  return { statusData }
}
