import { z } from 'zod'

import i18n from '@/utils/i18n'

const customErrorMap: z.ZodErrorMap = (issue, ctx) => {
  if (issue.code === z.ZodIssueCode.too_big) {
    if (issue.type === 'string') {
      return {
        message: i18n.t('errors.stringMaxLength', { value: issue.maximum }),
      }
    } else if (issue.type === 'number') {
      return { message: i18n.t('errors.maxValue', { value: issue.maximum }) }
    }
  }

  if (issue.code === z.ZodIssueCode.too_small) {
    if (issue.type === 'string') {
      return {
        message: i18n.t('errors.stringMinLength', { value: issue.minimum }),
      }
    }
  }

  if (issue.code === z.ZodIssueCode.invalid_type) {
    if (issue.expected === 'number') {
      return { message: i18n.t('errors.expectedNumber') }
    }
  }

  if (issue.code === z.ZodIssueCode.custom) {
    if (issue.params?.type === 'wrongRange' && issue.params?.range) {
      return {
        message: i18n.t('errors.wrongRange', { range: issue.params?.range }),
      }
    }

    if (issue.params?.type === 'wrongHorizon') {
      return { message: i18n.t('errors.wrongHorizon') }
    }

    if (issue.params?.type === 'incorrect_date_range') {
      return { message: i18n.t('errors.incorrectDateRange') }
    }
  }

  return { message: ctx.defaultError }
}

z.setErrorMap(customErrorMap)
