import { App } from '@/widgets/App/App'
import { createBrowserRouter } from 'react-router-dom'

import ControlIcon from '@/assets/icons/control.svg'
import ExplorerIcon from '@/assets/icons/explorer.svg'
import LogsIcon from '@/assets/icons/logs.svg'
import MainIcon from '@/assets/icons/main.svg'
import ScheduleIcon from '@/assets/icons/schedule.svg'
import SettingsIcon from '@/assets/icons/settings.svg'
import TelemetryIcon from '@/assets/icons/telemetry.svg'

type HeadRoutes = { [key: string]: { title: string; icon: string } }

export const headRoutes: HeadRoutes = {
  '/': {
    title: 'dynamic.route.main',
    icon: MainIcon,
  },
  '/telemetry': {
    title: 'dynamic.route.telemetry',
    icon: TelemetryIcon,
  },
  '/control': {
    title: 'dynamic.route.control',
    icon: ControlIcon,
  },
  '/schedule': {
    title: 'dynamic.route.schedule',
    icon: ScheduleIcon,
  },
  '/logs': {
    title: 'dynamic.route.logs',
    icon: LogsIcon,
  },
  '/explorer': {
    title: 'dynamic.route.explorer',
    icon: ExplorerIcon,
  },
  '/settings': {
    title: 'dynamic.route.settings',
    icon: SettingsIcon,
  },
}

export const routes = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    children: [
      {
        path: '/',
        lazy: () => import('@/pages/Main/MainPage'),
      },
      {
        path: '/telemetry',
        lazy: () => import('@/pages/Telemetry/TelemetryPage'),
      },
      {
        path: '/control',
        lazy: () => import('@/pages/Control/ControlPage'),
      },
      {
        path: '/schedule',
        lazy: () => import('@/pages/Schedule/SchedulePage'),
      },
      {
        path: '/logs',
        lazy: () => import('@/pages/Logs/LogsPage'),
      },
      {
        path: '/explorer',
        lazy: () => import('@/pages/Explorer/ExplorerPage'),
      },
      {
        path: '/settings',
        lazy: () => import('@/pages/Settings/SettingsPage'),
      },
    ],
  },
])

export enum MODALS {
  UnavailableControl = 'unavailable_control',
  UnavailableExplorer = 'unavailable_explorer',
  FlySettings = 'fly_settings',
  FlyInfo = 'fly_info',
  Login = 'login',
  LimitSettings = 'limit_settings',
}
