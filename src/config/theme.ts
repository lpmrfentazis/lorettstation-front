import { extendTheme } from '@mui/joy'

declare module '@mui/joy/styles' {
  interface Palette {}
}

export const theme = extendTheme({})
