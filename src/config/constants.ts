import { z } from 'zod'

import { getMapKey } from '@/utils/getMapKey'

const EnvConfigSchema = z.object({
  isDev: z.boolean(),
  apiMapKey: z.string().default(''),
  apiUrl: z.string().default(''),
  explorerUrl: z
    .string()
    .default('/kodexplorer/index.php?explorer/index&path='),

  telemetryUpdateTimeout: z.coerce.number().default(1000),
  logsUpdateTimeout: z.coerce.number().default(1000),
  scheduleUpdateTimeout: z.coerce.number().default(5000),
  settignsUpdateTimeout: z.coerce.number().default(60 * 1000),
  controlUpdateTimeout: z.coerce.number().default(500),

  defaultLogsLimit: z.number().default(3),
  defaultScheduleLimit: z.number().default(20),

  trajectoryStep: z.string().default('10'),
  trajectoryTimeInterval: z.number().default(60 * 60 * 1000 * 2),
})

export const envConfig = EnvConfigSchema.parse({
  isDev: import.meta.env.MODE === 'development',
  apiMapKey: import.meta.env.VITE_API_MAP_KEY,

  apiUrl: import.meta.env.VITE_API_URL,
  explorerUrl: import.meta.env.VITE_EXPLORER_URL,

  telemetryUpdateTimeout: import.meta.env.VITE_TELEMETRY_UPDATE_TIMEOUT,
  logsUpdateTimeout: import.meta.env.VITE_LOGS_UPDATE_TIMEOUT,
  scheduleUpdateTimeout: import.meta.env.VITE_SCHEDULE_UPDATE_TIMEOUT,
  settignsUpdateTimeout: import.meta.env.VITE_SETTINGS_UPDATE_TIMEOUT,
  controlUpdateTimeout: import.meta.env.VITE_CONTROL_UPDATE_TIMEOUT,
})

export const TIME_ZONE_OFFSET = new Date().getTimezoneOffset() * 60 * 1000

export const LIMIT_INCREMENT = 20

export const GLOBAL_COLORS = {
  '--danger-bg-color': {
    light: 'var(--joy-palette-warning-300, #F3C896)',
    dark: 'var(--joy-palette-warning-500, #9A5B13)',
  },
  '--calendar-picker-filter': {
    light: 'initial',
    dark: 'invert(1)',
  },
}

export const LANGUAGES = {
  ru: { text: 'Русский 🇷🇺' },
  en: { text: 'English 🇺🇸' },
  it: { text: 'Italiano 🇮🇹' },
}

export const MAP_TYPE = {
  schema: { i18n: 'dynamic.map.schemaView' },
  hybride: { i18n: 'dynamic.map.hybrideView' },
}

export type TMapLink = {
  [key in keyof typeof MAP_TYPE]: { light: string; dark: string }
}

export const MAP_LINK: TMapLink = {
  schema: {
    light: `https://tiles.stadiamaps.com/tiles/alidade_smooth/{z}/{x}/{y}{r}.png${getMapKey()}`,
    dark: `https://tiles.stadiamaps.com/tiles/alidade_smooth_dark/{z}/{x}/{y}{r}.png${getMapKey()}`,
  },
  hybride: {
    light: `https://tiles.stadiamaps.com/tiles/alidade_satellite/{z}/{x}/{y}{r}.jpg${getMapKey()}`,
    dark: `https://tiles.stadiamaps.com/tiles/alidade_satellite/{z}/{x}/{y}{r}.jpg${getMapKey()}`,
  },
}

export const COLORS = {
  text: {
    light: 0x171a1c,
    dark: 0xf0f4f8,
  },
}

export const TEXT_OPTIONS = {
  fontFamily: 'Roboto',
  fontSize: 32,
} as const

export const BOLD_TEXT_OPTIONS = {
  fontFamily: 'Roboto',
  fontSize: 36,
  fontWeight: 'bold',
} as const

export const POLAR_PALLETE = [
  { snr: 0, R: 204, G: 204, B: 204 }, // серый
  { snr: 3, R: 255, G: 0, B: 0 }, // красный
  { snr: 6, R: 255, G: 140, B: 0 }, // оранжевый
  { snr: 8, R: 255, G: 200, B: 0 }, // светло-оранжевый
  { snr: 10, R: 250, G: 210, B: 0 }, // желтый
  // { snr: 12, R: 170, G: 240, B: 0 }, // светло-зеленый
  { snr: 12, R: 40, G: 140, B: 70 }, // зеленый
  { snr: 14, R: 5, G: 100, B: 235 }, // синий
  { snr: 16, R: 90, G: 0, B: 255 }, // фиолетовый
  { snr: 24, R: 235, G: 0, B: 250 }, // розовый
  { snr: 36, R: 255, G: 255, B: 255 }, // белый
]
