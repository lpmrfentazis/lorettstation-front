import { Box } from '@mui/joy'

import { getDate, getTime } from '@/utils/date'

import { LogStatus } from '@/types/logs'

import { LogRowBackground, LogRowColor } from '../../constants'

export const LogRow = ({
  text,
  status,
  date,
}: {
  text: string
  date: string
  status: string
}) => {
  const formattedDate = new Date(date).getTime()

  return (
    <Box
      sx={{
        px: { sm: '16px', xs: '10px' },
        py: '5px',
        fontFamily: 'Roboto Mono',
        background: LogRowBackground[status as LogStatus],
        borderLeft: `3px solid ${LogRowColor[status as LogStatus]}`,
        borderRadius: '5px',
        color: 'var(--log-text-color)',
      }}
    >
      <pre style={{ overflowX: 'auto' }}>
        <code style={{ whiteSpace: 'pre-wrap' }}>
          {getDate(formattedDate)} {getTime(formattedDate)} |
          <span
            style={{
              color: LogRowColor[status as LogStatus],
              fontWeight: 'bold',
            }}
          >
            {status}
          </span>
          |: {text}
        </code>
      </pre>
    </Box>
  )
}
