import { Wrapper } from '@/ui/Container/Wrapper'
import { Loader } from '@/widgets/Loader/Loader'
import {
  Box,
  Button,
  Card,
  CircularProgress,
  IconButton,
  Stack,
  Typography,
} from '@mui/joy'
import { DownloadIcon } from '@primer/octicons-react'
import { AxiosProgressEvent } from 'axios'
import { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { toast } from 'sonner'

import { useLogs } from '@/api/logs'

import telemetryService from '@/services/telemetryService'

import { useLogsStore } from '@/store/logsStore'

import { useThemedColors } from '@/hooks/colorScheme'

import { MainLayout } from '../../widgets/Layouts/MainLayout'
import { LogRow } from './components/LogRow/LogRow'
import { colors } from './constants'

export const Component = () => {
  const { isLoading, isFetching } = useLogs()
  const [downloadProgress, setDownloadProgress] = useState<null | number>(null)

  const logs = useLogsStore(state => state.logs)
  const { t } = useTranslation()
  useThemedColors(colors)
  const incrementLimit = useLogsStore(state => state.incrementLimit)

  const downloadLogsFile = () => {
    toast.info(t('toast.downloadStart'))
    telemetryService.getLogsFile(onProgress)
  }

  const onProgress = (progress: AxiosProgressEvent) => {
    setDownloadProgress(progress.progress || 0)
    if (progress.progress === 1) {
      setTimeout(() => {
        setDownloadProgress(null)
      }, 1000)
    }
  }

  const loadMore = () => {
    incrementLimit()
  }

  return (
    <MainLayout>
      <Wrapper>
        <Box>
          <Stack
            direction={'row'}
            columnGap={'20px'}
            alignItems={'center'}
            justifyContent={'space-between'}
            sx={{ mb: '20px' }}
          >
            <Stack
              direction={'row'}
              alignItems={'center'}
              columnGap={'20px'}
            >
              <Typography level="h1">{t('logs.title')}</Typography>
              <IconButton
                onClick={downloadLogsFile}
                sx={{ width: '24px', height: '24px' }}
              >
                <DownloadIcon size={24} />
              </IconButton>
            </Stack>
            {downloadProgress !== null && (
              <CircularProgress
                color="success"
                determinate
                value={downloadProgress * 100}
                size="sm"
              ></CircularProgress>
            )}
          </Stack>
          <Card
            sx={{
              p: { xs: '16px', sm: '30px' },
              height: 'calc(100vh - var(--Header-height) - 100px)',
              overflowY: 'auto',
            }}
          >
            {isLoading && <Loader />}

            <Stack gap={'16px'}>
              {logs.length > 0 &&
                logs.map(item => {
                  return (
                    <LogRow
                      key={item.timeMoment}
                      date={item.timeMoment}
                      status={item.level}
                      text={item.message}
                    />
                  )
                })}
            </Stack>
            <Box
              sx={
                {
                  // position: 'fixed',
                  // bottom: '60px',
                }
              }
            >
              <Button
                onClick={loadMore}
                loading={isFetching}
              >
                {t('logs.loadMore')}
              </Button>
            </Box>
          </Card>
        </Box>
      </Wrapper>
    </MainLayout>
  )
}

Component.displayName = 'LogsPage'
