import { LogStatus } from '@/types/logs'

export const LogRowBackground = {
  [LogStatus.Info]: 'var(--log-info-bg)',
  [LogStatus.Error]: 'var(--log-error-bg)',
  [LogStatus.Warning]: 'var(--log-warning-bg)',
}

export const LogRowColor = {
  [LogStatus.Info]: 'var(--log-info-color)',
  [LogStatus.Error]: 'var(--log-error-color)',
  [LogStatus.Warning]: 'var(--log-warning-color)',
}

export const colors = {
  '--log-info-color': {
    light: 'var(--joy-palette-primary-500, #0B6BCB)',
    dark: 'var(--joy-palette-primary-800, #E3EFFB)',
  },

  '--log-warning-color': {
    light: 'var(--joy-palette-warning-400, #EA9A3E)',
    dark: 'var(--joy-palette-warning-800, #FDF0E1)',
  },
  '--log-error-color': {
    light: 'var(--joy-palette-danger-500, #C41C1C)',
    dark: 'var(--joy-palette-danger-800, #FCE4E4)',
  },
  '--log-info-bg': {
    light: 'var(--joy-palette-primary-100, #E3EFFB)',
    dark: 'var(--joy-palette-primary-500, #0B6BCB)',
  },
  '--log-warning-bg': {
    light: 'var(--joy-palette-warning-100, #FDF0E1)',
    dark: 'var(--joy-palette-warning-400, #EA9A3E)',
  },
  '--log-error-bg': {
    light: 'var(--joy-palette-danger-100, #FCE4E4)',
    dark: 'var(--joy-palette-danger-500, #C41C1C)',
  },
  '--log-text-color': {
    light: '#171A1C',
    dark: '#F0F4F8',
  },
}
