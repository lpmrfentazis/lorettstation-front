import { Divider, IconButton, Stack } from '@mui/joy'
import Box from '@mui/joy/Box'
import { GearIcon, SyncIcon } from '@primer/octicons-react'
import { useQueryClient } from '@tanstack/react-query'
import { useTranslation } from 'react-i18next'
import { toast } from 'sonner'

import { MODALS } from '@/config/routes'

import { useOpenModal } from '@/hooks/modal'

import { FlyType, TPassedFlySchema, TPlannedFlySchema } from '@/types/schedule'

import { ScheduleListItem } from './ScheduleListItem'

export const ScheduleList = ({
  passedData,
  plannedData,
}: {
  passedData: TPassedFlySchema[]
  plannedData: TPlannedFlySchema[]
}) => {
  const queryClient = useQueryClient()
  const { t } = useTranslation()
  const { openModal } = useOpenModal()
  const forceUpdateTable = () => {
    toast.info(t('toast.scheduleUpdate'))
    queryClient.invalidateQueries({
      queryKey: ['schedule'],
      refetchType: 'all',
    })
  }
  return (
    <Box>
      <Stack
        direction={'row'}
        sx={{ py: 2 }}
      >
        <IconButton
          sx={{ minWidth: 0, minHeight: 0 }}
          onClick={forceUpdateTable}
        >
          <SyncIcon size={14} />
        </IconButton>
        <IconButton
          sx={{ minWidth: 0, minHeight: 0 }}
          onClick={() => openModal(MODALS.LimitSettings)}
        >
          <GearIcon size={14} />
        </IconButton>
      </Stack>
      <Divider />
      {passedData.map(row => (
        <ScheduleListItem
          key={row.passID}
          type={FlyType.Passed}
          row={row}
        />
      ))}
      {plannedData.map(row => (
        <ScheduleListItem
          key={row.orbit}
          type={FlyType.Planned}
          row={row}
        />
      ))}
    </Box>
  )
}
