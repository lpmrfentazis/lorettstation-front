import {
  Box,
  Chip,
  Dropdown,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
} from '@mui/joy'
import { LockIcon, TriangleDownIcon, XIcon } from '@primer/octicons-react'
import { useState } from 'react'
import { useTranslation } from 'react-i18next'

import { useControlStore } from '@/store/controlStore'

import { FlyStatus } from '@/types/schedule'

import { FLY_STATUS_CHANGE, FLY_STATUS_DATA } from './constants'

export const FlyStatusChip = ({
  status: statusProp,
  canChange = true,
  onChange,
}: {
  status: FlyStatus
  canChange?: boolean
  onChange?: (status: FlyStatus) => void
}) => {
  const { t } = useTranslation()
  const [open, setOpen] = useState(false)
  const [status, setStatus] = useState(statusProp)
  const hasControlPanel = useControlStore(state => state.hasControlPanel)

  const onPress = () => {
    if (!canChange) return

    if (status === FlyStatus.Convoy) {
      if (!hasControlPanel || !onChange) return

      onChange(FlyStatus.Ignore)
      setStatus(FlyStatus.Ignore)
    } else {
      setOpen(prev => !prev)
    }
  }

  const onStatusChange = (newStatus: FlyStatus) => {
    if (!canChange) return

    if (status !== newStatus && onChange) {
      onChange(newStatus)
      setStatus(newStatus)
    }
  }

  const ChipDecorator = () => {
    if (status === FlyStatus.Finished) return null

    if (!canChange || (status === FlyStatus.Convoy && !hasControlPanel))
      return <LockIcon size={12} />

    if (status === FlyStatus.Convoy) {
      return <XIcon size={12} />
    }

    return (
      <Box
        sx={{
          transform: `rotate(${open ? 180 : 0}deg)`,
          transition: 'transform 0.2s ease-in-out',
        }}
      >
        {canChange && <TriangleDownIcon size={16} />}
      </Box>
    )
  }

  return (
    <Dropdown
      open={open}
      onOpenChange={onPress}
    >
      <MenuButton
        sx={{ cursor: canChange ? 'pointer' : 'default' }}
        variant="plain"
        slots={{ root: IconButton }}
        slotProps={{ root: { variant: 'plain', color: 'neutral', size: 'xs' } }}
      >
        <Chip
          variant="soft"
          size="sm"
          sx={{ cursor: canChange ? 'pointer' : 'default' }}
          endDecorator={<ChipDecorator />}
          color={FLY_STATUS_DATA[status].color}
        >
          {t(FLY_STATUS_DATA[status].i18n)}
        </Chip>
      </MenuButton>
      {status !== FlyStatus.Finished && canChange && (
        <Menu
          size="sm"
          sx={{ minWidth: 140 }}
        >
          {FLY_STATUS_CHANGE[status].map((item: FlyStatus, idx) => {
            const s = FLY_STATUS_DATA[item]
            return (
              <MenuItem
                key={idx}
                onClick={() => onStatusChange(item)}
                color={s.color}
              >
                {t(s.i18n)}
              </MenuItem>
            )
          })}
        </Menu>
      )}
    </Dropdown>
  )
}
