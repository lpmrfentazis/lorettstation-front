import { Wrapper } from '@/ui/Container/Wrapper'
import { colors } from '@/widgets/CodeHighlighter/constants'
import { Loader } from '@/widgets/Loader/Loader'
import { Box, Typography } from '@mui/joy'
import { useTranslation } from 'react-i18next'

import {
  useLastUpdateSchedule,
  usePassedSchedule,
  usePlannedSchedule,
} from '@/api/schedule'

import { useThemedColors } from '@/hooks/colorScheme'
import { useDeviceSize } from '@/hooks/size'

import { MainLayout } from '../../widgets/Layouts/MainLayout'
import { ScheduleList } from './ScheduleList'
import { ScheduleTable } from './ScheduleTable'

export const Component = () => {
  const { t } = useTranslation()
  useLastUpdateSchedule()
  const { passedData, isPassedLoading } = usePassedSchedule()
  const { plannedData, isPlannedLoading } = usePlannedSchedule()
  useThemedColors(colors)
  const { width } = useDeviceSize()

  return (
    <MainLayout>
      <Wrapper>
        <Box>
          <Typography
            level="h1"
            sx={{ mb: '20px' }}
          >
            {t('schedule.title')}
          </Typography>
          {(isPassedLoading || isPlannedLoading) && <Loader />}

          {passedData && plannedData && (
            <>
              {width > 800 ? (
                <ScheduleTable
                  passedData={passedData}
                  plannedData={plannedData}
                />
              ) : (
                <ScheduleList
                  passedData={passedData}
                  plannedData={plannedData}
                />
              )}
            </>
          )}
        </Box>
      </Wrapper>
    </MainLayout>
  )
}

Component.displayName = 'SchedulePage'
