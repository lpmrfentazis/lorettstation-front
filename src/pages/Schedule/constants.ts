import { ColorPaletteProp } from '@mui/joy'

import { FlyStatus } from '@/types/schedule'

type StatusData = {
  [key in FlyStatus]: {
    color: ColorPaletteProp
    i18n: string
  }
}

export const FLY_STATUS_DATA: StatusData = {
  [FlyStatus.Waiting]: {
    color: 'neutral',
    i18n: 'dynamic.flyStatus.Waiting',
  },
  [FlyStatus.Convoy]: {
    color: 'success',
    i18n: 'dynamic.flyStatus.Convoy',
  },
  [FlyStatus.Finished]: {
    color: 'primary',
    i18n: 'dynamic.flyStatus.Finished',
  },
  [FlyStatus.Ignore]: {
    color: 'danger',
    i18n: 'dynamic.flyStatus.Ignore',
  },
  [FlyStatus.Collision]: {
    color: 'warning',
    i18n: 'dynamic.flyStatus.Collision',
  },
}

export const FLY_STATUS_CHANGE = {
  [FlyStatus.Collision]: [FlyStatus.Ignore, FlyStatus.Collision],
  [FlyStatus.Ignore]: [FlyStatus.Waiting, FlyStatus.Ignore],
  [FlyStatus.Waiting]: [FlyStatus.Waiting, FlyStatus.Ignore],
  [FlyStatus.Convoy]: [],
  [FlyStatus.Finished]: [],
} as const
