import { IconButton, Sheet, Stack, Table, Typography } from '@mui/joy'
import { GearIcon, SyncIcon } from '@primer/octicons-react'
import { useQueryClient } from '@tanstack/react-query'
import { useTranslation } from 'react-i18next'
import { toast } from 'sonner'

import { MODALS } from '@/config/routes'

import { useOpenModal } from '@/hooks/modal'

import { FlyType, TPassedFlySchema, TPlannedFlySchema } from '@/types/schedule'

import { ScheduleTableRow } from './ScheduleTableRow'

export const ScheduleTable = ({
  passedData,
  plannedData,
}: {
  passedData: TPassedFlySchema[]
  plannedData: TPlannedFlySchema[]
}) => {
  const queryClient = useQueryClient()
  const { t } = useTranslation()
  const { openModal } = useOpenModal()

  const forceUpdateTable = () => {
    toast.info(t('toast.scheduleUpdate'))

    queryClient.invalidateQueries({
      queryKey: ['schedule'],
      refetchType: 'all',
    })
  }

  return (
    <>
      <Sheet
        variant="outlined"
        sx={{
          width: '100%',
          maxHeight: 'calc(100vh - var(--Header-height) - 100px)',
          borderRadius: 'sm',
          flexShrink: 1,
          overflow: 'auto',
          minHeight: 0,
        }}
      >
        <Table
          stickyHeader
          hoverRow
          sx={{
            '--TableCell-headBackground':
              'var(--joy-palette-background-level1)',
            '--Table-headerUnderlineThickness': '1px',
            '--TableRow-hoverBackground':
              'var(--joy-palette-background-level1)',
            '--TableCell-paddingY': '4px',
            '--TableCell-paddingX': '8px',
          }}
        >
          <thead>
            <tr>
              <th className="table-header">{t('schedule.satellite')}</th>
              <th className="table-header">{t('schedule.timeStart')}</th>
              <th className="table-header">{t('schedule.timeEnd')}</th>
              <th className="table-header">{t('schedule.culmination')}</th>
              <th className="table-header">{t('schedule.status')}</th>
              <th className="table-header">
                <Stack
                  direction={'row'}
                  justifyContent={'space-between'}
                  alignItems={'center'}
                  rowGap={'10px'}
                  flexWrap={'wrap'}
                >
                  <Typography>{t('schedule.settings')}</Typography>
                  <Stack direction={'row'}>
                    <IconButton
                      sx={{ minWidth: 0, minHeight: 0 }}
                      onClick={forceUpdateTable}
                    >
                      <SyncIcon size={14} />
                    </IconButton>
                    <IconButton
                      sx={{ minWidth: 0, minHeight: 0 }}
                      onClick={() => openModal(MODALS.LimitSettings)}
                    >
                      <GearIcon size={14} />
                    </IconButton>
                  </Stack>
                </Stack>
              </th>
            </tr>
          </thead>
          <tbody>
            {passedData.map(row => (
              <ScheduleTableRow
                key={row.passID}
                type={FlyType.Passed}
                row={row}
              />
            ))}
            {plannedData.map(row => (
              <ScheduleTableRow
                key={row.orbit}
                type={FlyType.Planned}
                row={row}
              />
            ))}
          </tbody>
        </Table>
      </Sheet>
    </>
  )
}
