import { IconButton, Link, Typography } from '@mui/joy'
import { GearIcon } from '@primer/octicons-react'

import { useUpdateSchedule } from '@/api/schedule'

import { MODALS } from '@/config/routes'

import { useCheckRights } from '@/hooks/checkRights'
import { useOpenModal } from '@/hooks/modal'

import { getDate, getTime } from '@/utils/date'

import {
  FlyStatus,
  FlyType,
  TPassedFlySchema,
  TPlannedFlySchema,
} from '@/types/schedule'

import { FlyStatusChip } from './FlyStatus'

type ScheduleTableRowProps =
  | {
      type: FlyType.Passed
      row: TPassedFlySchema
    }
  | {
      type: FlyType.Planned
      row: TPlannedFlySchema
    }

const DateTimeCell = ({ date }: { date: string }) => {
  const dt = new Date(date)
  return (
    <Typography level="body-xs">
      {getDate(dt.getTime())}
      <br />
      {getTime(dt.getTime())}
    </Typography>
  )
}

export const ScheduleTableRow = ({ type, row }: ScheduleTableRowProps) => {
  const hasAccessRights = useCheckRights(2)
  const { openModal } = useOpenModal()
  const { updateSchedule } = useUpdateSchedule()

  const updateFlyStatus = (data: FlyStatus) => {
    if (type === FlyType.Planned) {
      updateSchedule({ ...row, status: data })
    }
  }

  return (
    <tr
      style={{
        background:
          type === FlyType.Planned && row.status === FlyStatus.Collision
            ? 'var(--danger-bg-color)'
            : '',
      }}
    >
      <td>
        {type === FlyType.Passed ? (
          <Link
            onClick={() => openModal(MODALS.FlyInfo, { passID: row.passID })}
          >
            {row.satellite}
          </Link>
        ) : (
          row.satName
        )}
      </td>
      <td>
        <DateTimeCell
          date={type === FlyType.Passed ? row.passInfo.timeEnd : row.timeStart}
        />
      </td>
      <td>
        <DateTimeCell
          date={type === FlyType.Passed ? row.passInfo.timeEnd : row.timeEnd}
        />
      </td>
      <td>
        <Typography level="body-xs">
          {type === FlyType.Passed
            ? row.passInfo.culmination.toFixed(2)
            : row.culmination.toFixed(2)}
          °
        </Typography>
      </td>
      <td>
        {type === FlyType.Passed ? (
          <FlyStatusChip status={FlyStatus.Finished} />
        ) : (
          <FlyStatusChip
            status={row.status}
            onChange={updateFlyStatus}
            canChange={hasAccessRights}
          />
        )}
      </td>
      <td>
        {type === FlyType.Planned && (
          <IconButton onClick={() => openModal(MODALS.FlySettings, row)}>
            <GearIcon size={16} />
          </IconButton>
        )}
      </td>
    </tr>
  )
}
