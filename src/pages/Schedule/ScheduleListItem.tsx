import {
  IconButton,
  Link,
  List,
  ListDivider,
  ListItem,
  Stack,
  Typography,
} from '@mui/joy'
import { GearIcon } from '@primer/octicons-react'
import { useTranslation } from 'react-i18next'

import { useUpdateSchedule } from '@/api/schedule'

import { MODALS } from '@/config/routes'

import { useCheckRights } from '@/hooks/checkRights'
import { useOpenModal } from '@/hooks/modal'

import { getDate, getTime } from '@/utils/date'

import {
  FlyStatus,
  FlyType,
  TPassedFlySchema,
  TPlannedFlySchema,
} from '@/types/schedule'

import { FlyStatusChip } from './FlyStatus'

type ScheduleListItemProps =
  | {
      type: FlyType.Passed
      row: TPassedFlySchema
    }
  | {
      type: FlyType.Planned
      row: TPlannedFlySchema
    }

const DateTimeCell = ({ date }: { date: string }) => {
  const dt = new Date(date)
  return (
    <Typography>
      {getDate(dt.getTime())} {getTime(dt.getTime())}
    </Typography>
  )
}

export const ScheduleListItem = ({ type, row }: ScheduleListItemProps) => {
  const { t } = useTranslation()
  const hasAccessRights = useCheckRights(2)

  const { openModal } = useOpenModal()
  const { updateSchedule } = useUpdateSchedule()

  const updateFlyStatus = (data: FlyStatus) => {
    if (type === FlyType.Planned) {
      updateSchedule({ ...row, status: data })
    }
  }

  return (
    <List
      size="sm"
      sx={{
        '--ListItem-paddingX': 0,
      }}
    >
      <ListItem sx={{ display: 'block' }}>
        <Stack
          direction={'column'}
          gap={'6px'}
        >
          {type === FlyType.Passed ? (
            <Link
              fontWeight={'bold'}
              onClick={() => openModal(MODALS.FlyInfo, { passID: row.passID })}
            >
              {row.satellite}
            </Link>
          ) : (
            <Typography fontWeight={'bold'}>{row.satName}</Typography>
          )}
          <Stack
            direction={'row'}
            justifyContent={'space-between'}
          >
            <Typography fontWeight={'bold'}>
              {t('schedule.timeStart')}
            </Typography>
            <DateTimeCell
              date={
                type === FlyType.Passed ? row.passInfo.timeEnd : row.timeStart
              }
            />
          </Stack>
          <Stack
            direction={'row'}
            justifyContent={'space-between'}
          >
            <Typography fontWeight={'bold'}>{t('schedule.timeEnd')}</Typography>
            <DateTimeCell
              date={
                type === FlyType.Passed ? row.passInfo.timeEnd : row.timeEnd
              }
            />
          </Stack>
          <Stack
            direction={'row'}
            justifyContent={'space-between'}
          >
            <Typography fontWeight={'bold'}>
              {t('schedule.culmination')}
            </Typography>
            <Typography>
              {type === FlyType.Passed
                ? row.passInfo.culmination.toFixed(2)
                : row.culmination.toFixed(2)}
            </Typography>
          </Stack>
          <Stack
            direction={'row'}
            justifyContent={'space-between'}
          >
            <Typography fontWeight={'bold'}>{t('schedule.status')}</Typography>
            {type === FlyType.Passed ? (
              <FlyStatusChip status={FlyStatus.Finished} />
            ) : (
              <FlyStatusChip
                status={row.status}
                onChange={updateFlyStatus}
                canChange={hasAccessRights}
              />
            )}
          </Stack>
          {type === FlyType.Planned && (
            <Stack
              direction={'row'}
              justifyContent={'space-between'}
            >
              <Typography fontWeight={'bold'}>
                {t('schedule.settings')}
              </Typography>
              <IconButton onClick={() => openModal(MODALS.FlySettings, row)}>
                <GearIcon size={16} />
              </IconButton>
            </Stack>
          )}
        </Stack>
      </ListItem>
      <ListDivider />
    </List>
  )
}
