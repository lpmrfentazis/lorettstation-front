import { Container } from '@/ui/Container/Container'
import { Loader } from '@/widgets/Loader/Loader'
import { Box, Grid, Typography } from '@mui/joy'
import { useTranslation } from 'react-i18next'

import { useTelemetry } from '@/api/telemetry'

import CollapsibleTable from '../../widgets/CollapsedTable/CollapsibleTable'
import { MainLayout } from '../../widgets/Layouts/MainLayout'

export const Component = () => {
  const { data, isLoading } = useTelemetry()
  const { t } = useTranslation()

  return (
    <MainLayout>
      <Container>
        <Box>
          <Typography
            level="h1"
            sx={{ mb: '30px' }}
          >
            {t('telemetry.title')}
          </Typography>
          <Grid
            container
            spacing={'30px'}
          >
            {isLoading && <Loader />}
            {data &&
              Object.entries(data).map(([key, values]) => {
                if (values.type === 'table') {
                  return (
                    <Grid
                      md={6}
                      key={key}
                    >
                      <CollapsibleTable
                        table={values}
                        title={key}
                      />
                    </Grid>
                  )
                }

                // Тут можно добавлять отрисовку новых типов с бека
                return null
              })}
          </Grid>
        </Box>
      </Container>
    </MainLayout>
  )
}
