import { Input } from '@/widgets/Input/Input'
import { zodResolver } from '@hookform/resolvers/zod'
import { Button, Card, Checkbox, Grid } from '@mui/joy'
import { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { z } from 'zod'

import {
  useMovementsAbsolute,
  useMovementsCalibrate,
  useMovementsRelative,
  useMovementsStartPosition,
  useMovementsStop,
} from '@/api/control'

import { useCheckRights } from '@/hooks/checkRights'

import { TControlSchema } from '@/types/control'
import {
  AzimuthCorrectionSchema,
  AzimuthSchema,
  ElevationSchema,
  LatitudeSchema,
} from '@/types/global'

import { LastRow } from './LastRow'

export const ControlPanel = () => {
  const { t } = useTranslation()
  const hasAccessRights = useCheckRights(2)
  const [relative, setRelative] = useState(false)

  const {
    register,
    trigger,
    formState: { errors, isValid },
    getValues,
  } = useForm<TControlSchema>({
    resolver: zodResolver(
      z.object({
        azimuth: relative ? AzimuthCorrectionSchema : AzimuthSchema,
        elevation: relative ? LatitudeSchema : ElevationSchema,
      }),
    ),
    defaultValues: {
      azimuth: 0,
      elevation: 0,
    },
  })

  useEffect(() => {
    trigger()
  }, [relative])

  const { relativeMove, isRelativePending } = useMovementsRelative()
  const { absoluteMove, isAbsolutePending } = useMovementsAbsolute()
  const { stopMove, isStopPending } = useMovementsStop()
  const { calibrateMove, isCalibratePending } = useMovementsCalibrate()
  const { startPosition, isStartPositionPending } = useMovementsStartPosition()

  return (
    <Card>
      <form>
        <Grid
          container
          rowSpacing={'19px'}
          columnSpacing={{ md: '30px' }}
        >
          <Grid
            sm={6}
            xs={12}
          >
            <Input
              label={t('control.azimuth')}
              {...register('azimuth', { valueAsNumber: true })}
              errorLabel={errors.azimuth?.message}
            />
          </Grid>
          <Grid
            sm={6}
            xs={12}
          >
            <Input
              label={t('control.elevation')}
              {...register('elevation', { valueAsNumber: true })}
              errorLabel={errors.elevation?.message}
            />
          </Grid>
          <Grid
            sm={12}
            xs={12}
          >
            <Checkbox
              label={t('control.relativePosition')}
              checked={relative}
              onChange={() => setRelative(prev => !prev)}
            />
          </Grid>
          <Grid
            sm={6}
            xs={12}
          >
            <Button
              loading={isRelativePending || isAbsolutePending}
              disabled={!hasAccessRights}
              sx={{ width: '100%' }}
              onClick={() => {
                if (isValid) {
                  relative
                    ? relativeMove(getValues())
                    : absoluteMove(getValues())
                } else {
                  trigger(['azimuth', 'elevation'])
                }
              }}
            >
              {t('control.move')}
            </Button>
          </Grid>
          <Grid
            sm={6}
            xs={12}
          >
            <Button
              loading={isStopPending}
              disabled={!hasAccessRights}
              sx={{ width: '100%' }}
              onClick={() => stopMove()}
            >
              {t('control.stop')}
            </Button>
          </Grid>
          <Grid
            sm={6}
            xs={12}
          >
            <Button
              loading={isCalibratePending}
              disabled={!hasAccessRights}
              sx={{ width: '100%' }}
              onClick={() => calibrateMove()}
            >
              {t('control.calibration')}
            </Button>
          </Grid>
          <Grid
            sm={6}
            xs={12}
          >
            <Button
              loading={isStartPositionPending}
              disabled={!hasAccessRights}
              sx={{ width: '100%' }}
              onClick={() => startPosition()}
            >
              {t('control.startPosition')}
            </Button>
          </Grid>
          <LastRow />
        </Grid>
      </form>
    </Card>
  )
}
