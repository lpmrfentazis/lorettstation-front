import { Input } from '@/widgets/Input/Input'
import { zodResolver } from '@hookform/resolvers/zod'
import { Button, Divider, Grid } from '@mui/joy'
import { t } from 'i18next'
import { useForm } from 'react-hook-form'
import { z } from 'zod'

import { useMovementsSaveLandmark } from '@/api/control'

import { useCheckRights } from '@/hooks/checkRights'

import { AzimuthSchema } from '@/types/global'

const LandmarkAzimuthSchema = z.object({
  value: AzimuthSchema,
})

type TLandmarkAzimuth = z.infer<typeof LandmarkAzimuthSchema>

export const LastRow = () => {
  const hasAccessRights = useCheckRights(2)
  const { saveLandmark, isSaveLandmarkPending } = useMovementsSaveLandmark()
  const {
    register,
    trigger,
    formState: { errors, isValid },
    getValues,
  } = useForm<TLandmarkAzimuth>({
    resolver: zodResolver(LandmarkAzimuthSchema),
  })

  return (
    <>
      <Grid xs={12}>
        <Divider />
      </Grid>
      <Grid
        sm={6}
        xs={12}
      >
        <Input
          label={t('control.landmarkAzimuth')}
          {...register('value', { valueAsNumber: true })}
          errorLabel={errors.value?.message}
        />
      </Grid>
      <Grid
        sm={6}
        xs={12}
        sx={{ display: 'flex', alignItems: 'flex-end' }}
      >
        <Button
          loading={isSaveLandmarkPending}
          disabled={!hasAccessRights}
          sx={{ width: '100%' }}
          onClick={() => {
            if (isValid) {
              saveLandmark(getValues())
            } else {
              trigger('value')
            }
          }}
        >
          {t('control.saveLandmark')}
        </Button>
      </Grid>
    </>
  )
}
