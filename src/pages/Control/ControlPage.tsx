import { Container } from '@/ui/Container/Container'
import { Azimuth } from '@/widgets/Azimuth/Azimuth'
import {
  Box,
  Card,
  Divider,
  GlobalStyles,
  Grid,
  Stack,
  Typography,
} from '@mui/joy'
import { useEffect } from 'react'
import { useTranslation } from 'react-i18next'

import { useControl } from '@/api/control'

import { MODALS } from '@/config/routes'

import { useOpenModal } from '@/hooks/modal'

import { MainLayout } from '../../widgets/Layouts/MainLayout'
import { ControlPanel } from './components/ControlPanel/ControlPanel'

export const Component = () => {
  const { t } = useTranslation()

  const { controlData } = useControl()
  const { openModal } = useOpenModal()

  useEffect(() => {
    if (!controlData) return

    if (controlData.azimuth === null || controlData.elevation === null) {
      openModal(MODALS.UnavailableControl, {}, true)
    }
  }, [controlData, openModal])

  return (
    <MainLayout>
      <Container>
        <Box>
          <GlobalStyles
            styles={theme => ({
              ':root': {
                '--Is-azimuth-shown': 'none',
                [theme.breakpoints.up('sm')]: {
                  '--Is-azimuth-shown': 'block',
                },
              },
            })}
          />
          <Typography
            level="h1"
            sx={{ mb: '20px' }}
          >
            {t('control.title')}
          </Typography>
          <Grid
            container
            spacing={'30px'}
          >
            <Grid
              md={12}
              lg={6}
            >
              <ControlPanel />
            </Grid>
            <Grid
              xs={12}
              lg={6}
            >
              {controlData &&
                controlData.azimuth !== null &&
                controlData.elevation !== null && (
                  <Stack sx={{ width: '100%' }}>
                    <Box sx={{ display: 'var(--Is-azimuth-shown)' }}>
                      <Azimuth
                        azimuth={controlData.azimuth}
                        elevation={controlData.elevation}
                      />
                    </Box>
                    <Card>
                      <Grid
                        container
                        textAlign={'center'}
                        rowSpacing={'5px'}
                      >
                        <Grid xs={6}>
                          <Typography fontWeight={'bold'}>
                            {t('control.azimuth')}
                          </Typography>
                        </Grid>
                        <Grid xs={6}>
                          <Typography fontWeight={'bold'}>
                            {t('control.elevation')}
                          </Typography>
                        </Grid>
                        <Grid xs={12}>
                          <Divider />
                        </Grid>
                        <Grid xs={6}>
                          <Typography>{controlData.azimuth}°</Typography>
                        </Grid>
                        <Grid xs={6}>
                          <Typography>{controlData.elevation}°</Typography>
                        </Grid>
                      </Grid>
                    </Card>
                  </Stack>
                )}
            </Grid>
          </Grid>
        </Box>
      </Container>
    </MainLayout>
  )
}

Component.displayName = 'ControlPage'
