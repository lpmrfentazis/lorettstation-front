import { Container } from '@/ui/Container/Container'
import { MainLayout } from '@/widgets/Layouts/MainLayout'
import {
  Box,
  Card,
  Tab,
  TabList,
  TabPanel,
  Tabs,
  Typography,
  tabClasses,
} from '@mui/joy'
import { useTranslation } from 'react-i18next'

import { Tab1 } from './components/Tabs/Tab1'
import { Tab2 } from './components/Tabs/Tab2'
import { Tab3 } from './components/Tabs/Tab3'

export const Component = () => {
  const { t } = useTranslation()

  return (
    <MainLayout>
      <Container>
        <Box>
          <Typography
            level="h1"
            sx={{ mb: '20px' }}
          >
            {t('settings.title')}
          </Typography>
          <Card>
            <Tabs
              defaultValue={0}
              sx={{
                bgcolor: 'transparent',
              }}
            >
              <TabList
                tabFlex={1}
                size="sm"
                sx={{
                  px: 0,
                  justifyContent: 'left',
                  [`&& .${tabClasses.root}`]: {
                    fontWeight: '600',
                    flex: 'initial',
                    color: 'text.tertiary',
                    [`&.${tabClasses.selected}`]: {
                      bgcolor: 'transparent',
                      color: 'text.primary',
                      '&::after': {
                        height: '2px',
                        bgcolor: 'primary.500',
                      },
                    },
                  },
                }}
              >
                <Tab
                  sx={{ borderRadius: '6px 6px 0 0' }}
                  indicatorInset
                  value={0}
                >
                  {t('settings.category.general')}
                </Tab>
                <Tab
                  sx={{ borderRadius: '6px 6px 0 0' }}
                  indicatorInset
                  value={1}
                >
                  {t('settings.category.satellites')}
                </Tab>
                <Tab
                  sx={{ borderRadius: '6px 6px 0 0' }}
                  indicatorInset
                  value={2}
                >
                  {t('settings.category.ui')}
                </Tab>
              </TabList>
              <TabPanel
                value={0}
                sx={{ px: 0 }}
              >
                <Tab1 />
              </TabPanel>
              <TabPanel
                value={1}
                sx={{ px: 0 }}
              >
                <Tab2 />
              </TabPanel>
              <TabPanel
                value={2}
                sx={{ px: 0 }}
              >
                <Tab3 />
              </TabPanel>
            </Tabs>
          </Card>
        </Box>
      </Container>
    </MainLayout>
  )
}

Component.displayName = 'SettingsPage'
