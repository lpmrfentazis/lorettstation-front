import { Select } from '@/widgets/Select/Select'
import { Box, Grid, Stack } from '@mui/joy'
import { useCallback } from 'react'
import { useTranslation } from 'react-i18next'

import { LANGUAGES } from '@/config/constants'

import { useToggleColorScheme } from '@/hooks/colorScheme'
import { useLanguage } from '@/hooks/lang'

import type { LangMode, ThemeMode } from '@/types/main'

const ThemeVariants = {
  light: { text: 'Светлая', i18n: 'dynamic.select.theme.light' },
  dark: { text: 'Темная', i18n: 'dynamic.select.theme.dark' },
}

export const Tab3 = () => {
  const { mode, toggleColor } = useToggleColorScheme()
  const { lang, setLang } = useLanguage()
  const { t } = useTranslation()

  const onChangeColor = useCallback(
    (newMode: ThemeMode) => {
      if (newMode !== mode) {
        toggleColor()
      }
    },
    [mode, toggleColor],
  )

  return (
    <Stack
      direction={'column'}
      gap={'20px'}
      sx={{ minHeight: '500px' }}
    >
      <Box sx={{ flex: '1 1 auto' }}>
        <Grid
          container
          rowSpacing={{ xs: '12px', sm: '17px' }}
          columnSpacing={{ xs: '12px', sm: '30px' }}
        >
          <Grid
            sm={6}
            xs={12}
          >
            <Select<LangMode>
              label={t('settings.language')}
              values={LANGUAGES}
              currentValue={lang}
              onChange={newLang => setLang(newLang)}
            />
          </Grid>
          <Grid
            sm={6}
            xs={12}
          >
            <Select<ThemeMode>
              label={t('settings.theme')}
              values={ThemeVariants}
              currentValue={mode}
              onChange={onChangeColor}
            />
          </Grid>
        </Grid>
      </Box>
    </Stack>
  )
}
