import { Input } from '@/widgets/Input/Input'
import { zodResolver } from '@hookform/resolvers/zod'
import { Box, Button, Grid, Stack } from '@mui/joy'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import { useSettings, useUpdateSettings } from '@/api/settings'

import { useCheckRights } from '@/hooks/checkRights'

import { SettingsSchema, TSettingsSchema } from '@/types/settings'

export const Tab1 = () => {
  const { t } = useTranslation()
  const hasAccessRights = useCheckRights(1)

  const { settingsData } = useSettings()
  const { updateSettings, isPending } = useUpdateSettings()
  const {
    register,
    handleSubmit,
    formState: { dirtyFields, errors },
    reset,
  } = useForm<TSettingsSchema>({
    resolver: zodResolver(SettingsSchema),
    values: settingsData,
  })

  const onSave = (data: TSettingsSchema) => {
    updateSettings(data)
  }

  return (
    <form onSubmit={handleSubmit(onSave)}>
      <Stack
        direction={'column'}
        gap={'20px'}
        sx={{ minHeight: '500px' }}
      >
        <Box sx={{ flex: '1 1 auto' }}>
          <Grid
            container
            rowSpacing={{ xs: '12px', sm: '12px' }}
            columnSpacing={{ xs: '12px', sm: '30px' }}
          >
            <Grid
              sm={6}
              xs={12}
            >
              <Input
                label={t('settings.stationName')}
                {...register('stationName')}
                errorLabel={errors.stationName?.message}
              />
            </Grid>
            <Grid
              sm={6}
              xs={12}
            >
              <Input
                label={t('settings.latitude')}
                {...register('lat', { valueAsNumber: true })}
                errorLabel={errors.lat?.message}
              />
            </Grid>
            <Grid
              sm={6}
              xs={12}
            >
              <Input
                label={t('settings.longitude')}
                {...register('lon', { valueAsNumber: true })}
                errorLabel={errors.lon?.message}
              />
            </Grid>
            <Grid
              sm={6}
              xs={12}
            >
              <Input
                label={t('settings.horizon')}
                {...register('horizon', { valueAsNumber: true })}
                errorLabel={errors.horizon?.message}
              />
            </Grid>
            <Grid
              sm={6}
              xs={12}
            >
              <Input
                label={t('settings.altitude')}
                {...register('alt', { valueAsNumber: true })}
                errorLabel={errors.alt?.message}
              />
            </Grid>
            <Grid
              sm={6}
              xs={12}
            >
              <Input
                label={t('settings.minApogee')}
                {...register('minApogee', { valueAsNumber: true })}
                errorLabel={errors.minApogee?.message}
              />
            </Grid>
            {/* <Grid sm={6} xs={12}>
            <Select label={t('settings.operatingMode')} values={EnergoVariants} />
          </Grid> */}
            <Grid
              sm={6}
              xs={12}
            >
              <Input
                label={t('settings.azimuth')}
                {...register('azimuthCorrection', { valueAsNumber: true })}
                errorLabel={errors.azimuthCorrection?.message}
              />
            </Grid>
          </Grid>
        </Box>
        <Box>
          {dirtyFields && (
            <Stack
              direction={'row'}
              gap={{ xs: '12px', sm: '30px' }}
            >
              <Button
                sx={{ width: '100%' }}
                type="submit"
                loading={isPending}
                disabled={!hasAccessRights}
              >
                {t('main.save')}
              </Button>
              <Button
                sx={{ width: '100%' }}
                variant="outlined"
                onClick={() => reset()}
              >
                {t('main.cancel')}
              </Button>
            </Stack>
          )}
        </Box>
      </Stack>
    </form>
  )
}
