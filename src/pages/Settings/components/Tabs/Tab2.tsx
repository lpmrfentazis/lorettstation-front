import { Loader } from '@/widgets/Loader/Loader'
import { SatellitesWidget } from '@/widgets/SatellitesWidget/SatellitesWidget'
import { Box, Button, Stack } from '@mui/joy'
import { t } from 'i18next'
import { useEffect, useState } from 'react'

import {
  useSatlistActive,
  useSatlistFull,
  useUpdateSatlist,
} from '@/api/settings'

import { useCheckRights } from '@/hooks/checkRights'

export const Tab2 = () => {
  const hasAccessRights = useCheckRights(2)

  const { fullData } = useSatlistFull()
  const { acitveData } = useSatlistActive()
  const { updateSatlist } = useUpdateSatlist()
  const [activeList, setActiveList] = useState<string[]>([])

  useEffect(() => {
    if (!acitveData) return
    setActiveList(acitveData)
  }, [acitveData])

  const onUpdateClick = () => {
    updateSatlist(activeList)
  }

  return (
    <Stack
      direction={'column'}
      gap={'20px'}
    >
      <Box sx={{ flex: '1 1 auto' }}>
        {fullData && acitveData ? (
          <SatellitesWidget
            leftColumnn={fullData}
            rightColumn={activeList}
            setActiveList={setActiveList}
          />
        ) : (
          <Loader />
        )}
      </Box>
      <Box>
        <Stack
          direction={'row'}
          gap={{ xs: '12px', sm: '30px' }}
        >
          <Button
            sx={{ width: '100%' }}
            onClick={onUpdateClick}
            disabled={!hasAccessRights}
          >
            {t('main.save')}
          </Button>
          <Button
            sx={{ width: '100%' }}
            variant="outlined"
            onClick={() => setActiveList(acitveData || [])}
          >
            {t('main.cancel')}
          </Button>
        </Stack>
      </Box>
    </Stack>
  )
}
