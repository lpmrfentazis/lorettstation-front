import { Wrapper } from '@/ui/Container/Wrapper'
import { MainLayout } from '@/widgets/Layouts/MainLayout'
import { Box, Grid, Stack, Typography } from '@mui/joy'
import { Suspense, lazy } from 'react'
import { useTranslation } from 'react-i18next'

import { useSettings } from '@/api/settings'

import { MapSchema } from '@/types/main'

import { AntennaForm } from './components/AntennaForm/AntennaForm'

const MapWrapper = lazy(() => import('./components/Map/MapWrapper'))

export const Component = () => {
  const { t } = useTranslation()

  const { settingsData } = useSettings()

  return (
    <MainLayout>
      <Wrapper>
        <Box>
          <Typography
            level="h1"
            sx={{ mb: '20px' }}
          >
            {t('main.title')}
          </Typography>
          <Grid
            container
            rowSpacing={'50px'}
            columnSpacing={'50px'}
            sx={{ mb: '15px' }}
          >
            <Grid sm={6}>
              <AntennaForm settingsData={settingsData} />
            </Grid>
          </Grid>

          <Stack direction={'column'}>
            <Typography
              fontWeight={'bold'}
              sx={{ fontSize: '20px', mb: '16px' }}
            >
              {t('main.mapTitle')}
            </Typography>
            <Suspense>
              {settingsData && (
                <MapWrapper settingsData={MapSchema.parse(settingsData)} />
              )}
            </Suspense>
          </Stack>
        </Box>
      </Wrapper>
    </MainLayout>
  )
}

Component.displayName = 'MainPage'
