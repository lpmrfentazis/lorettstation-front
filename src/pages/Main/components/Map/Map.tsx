import { Icon } from 'leaflet'
import DefaultIcon from 'leaflet/dist/images/marker-icon.png'
import { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Marker, Polyline, Popup, TileLayer, useMapEvents } from 'react-leaflet'

import { MAP_LINK } from '@/config/constants'

import { useMapStore } from '@/store/mapStore'

import { useToggleColorScheme } from '@/hooks/colorScheme'

import { calculate } from '@/utils/calculateAzimuth'

import StationIcon from '@/assets/images/station.png'

import { LatLng } from '@/types/main'

import { SatelliteTrajectory } from './SatelliteTrajectory'

const stationIcon = new Icon({
  iconUrl: StationIcon,
  iconSize: [40, 40],
})

const defaultIcon = new Icon({
  iconUrl: DefaultIcon,
  iconSize: [24, 42],
  iconAnchor: [12, 42],
})

export const Map = ({
  stationName,
  stationPos,
}: {
  stationName: string
  stationPos: LatLng
}) => {
  const { t } = useTranslation()
  const { mode } = useToggleColorScheme()
  const [computed, setComputed] = useState<
    { alpha: number; S: number } | undefined
  >()

  const {
    mapMode,
    setMapInstance,
    extraPos,
    setExtraPos,
    activeSalist,
    setShownSatellite,
  } = useMapStore(
    ({
      mapMode,
      setMapInstance,
      extraPos,
      setExtraPos,
      activeSalist,
      setShownSatellite,
    }) => ({
      mapMode,
      setMapInstance,
      extraPos,
      setExtraPos,
      activeSalist,
      setShownSatellite,
    }),
  )

  const mapInstance = useMapEvents({
    click: () => {
      setShownSatellite(null)
    },
    contextmenu: e => {
      setExtraPos(e.latlng)
      setComputed(
        calculate(stationPos.lat, stationPos.lng, e.latlng.lat, e.latlng.lng),
      )
    },
  })

  useEffect(() => {
    if (!mapInstance) return
    setMapInstance(mapInstance)
  }, [mapInstance, setMapInstance])

  return (
    <>
      <TileLayer
        url={MAP_LINK[mapMode][mode]}
        keepBuffer={Infinity}
      />
      <Marker
        position={stationPos}
        icon={stationIcon}
      >
        <Popup>
          {stationName}
          <br />
          lat: {stationPos.lat}°
          <br />
          lon: {stationPos.lng}°
        </Popup>
      </Marker>
      {extraPos &&
        extraPos.lat !== stationPos.lat &&
        extraPos.lng !== stationPos.lng && (
          <Marker
            position={extraPos}
            icon={defaultIcon}
          >
            <Popup>
              lat: {extraPos.lat}°
              <br />
              lon: {extraPos.lng}°
              <br />
              {t('main.map.azimuth')}: {computed?.alpha.toFixed(2)}°
              <br />
              {t('main.map.distance')}: {computed?.S.toFixed(2)}m
            </Popup>
          </Marker>
        )}
      {extraPos && (
        <Polyline
          pathOptions={{ color: '#add8e6' }}
          positions={[stationPos, extraPos]}
        />
      )}
      {activeSalist.map((item, idx) => {
        return (
          <SatelliteTrajectory
            key={idx}
            satellite={item}
          />
        )
      })}
    </>
  )
}
