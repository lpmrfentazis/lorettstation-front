import { Select } from '@/widgets/Select/Select'
import { Box, Button, Stack } from '@mui/joy'
import { useTranslation } from 'react-i18next'

import { Satellite } from '@/pages/Main/components/Satellite/Satellite'

import { MAP_TYPE } from '@/config/constants'

import { useMapStore } from '@/store/mapStore'

export const MapControlPanel = () => {
  const { t } = useTranslation()
  const { mapMode, changeMapMode, mapInstance, stationPos } = useMapStore(
    ({ mapMode, changeMapMode, mapInstance, stationPos }) => ({
      mapMode,
      changeMapMode,
      mapInstance,
      stationPos,
    }),
  )

  const onZoomInClick = () => {
    if (!mapInstance || !stationPos) return
    mapInstance.flyTo(stationPos, 18)
  }

  const onZoomOutClick = () => {
    if (!mapInstance || !stationPos) return
    mapInstance.flyTo(stationPos, 2)
  }

  return (
    <Stack
      columnGap={'15px'}
      rowGap={'15px'}
      alignItems={'flex-start'}
      direction={'row'}
      sx={{ py: '10px' }}
      flexWrap={'wrap'}
    >
      <Select
        values={MAP_TYPE}
        currentValue={mapMode}
        onChange={changeMapMode}
      />
      <Satellite />
      <Box>
        <Button
          variant="outlined"
          onClick={onZoomInClick}
        >
          {t('main.map.zoomIn')}
        </Button>
      </Box>

      <Box>
        <Button
          variant="outlined"
          onClick={onZoomOutClick}
        >
          {t('main.map.zoomOut')}{' '}
        </Button>
      </Box>
    </Stack>
  )
}
