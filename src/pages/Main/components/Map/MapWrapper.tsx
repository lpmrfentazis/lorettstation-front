import { AspectRatio, Box, Stack } from '@mui/joy'
import { LatLngBounds } from 'leaflet'
import 'leaflet/dist/leaflet.css'
import { useEffect } from 'react'
import { MapContainer } from 'react-leaflet'

import { useMapStore } from '@/store/mapStore'

import { TMapSchema } from '@/types/main'

import { Map } from './Map'
import { MapControlPanel } from './MapControlPanel'

const MAP_BOUNDS = new LatLngBounds(
  { lat: -90, lng: -360 },
  { lat: 90, lng: 360 },
)

const MapWrapper = ({
  settingsData: { stationName, stationPos },
}: {
  settingsData: TMapSchema
}) => {
  useEffect(() => {
    document.querySelector('.leaflet-control-attribution')?.remove()
  }, [])

  const { setStationPos } = useMapStore(({ setStationPos }) => ({
    setStationPos,
  }))

  useEffect(() => {
    setStationPos(stationPos)
  }, [setStationPos, stationName, stationPos])

  return (
    <Stack
      sx={{ width: '100%', height: '100%', mb: '50px' }}
      rowGap={'15px'}
    >
      <MapControlPanel />
      <AspectRatio
        ratio={4 / 3}
        maxHeight={'calc(100vh - 200px)'}
      >
        <Box sx={{ width: '100%', height: '100%' }}>
          <MapContainer
            minZoom={1}
            center={stationPos}
            zoom={2}
            maxBounds={MAP_BOUNDS}
          >
            <Map
              stationName={stationName}
              stationPos={stationPos}
            />
          </MapContainer>
        </Box>
      </AspectRatio>
    </Stack>
  )
}

export default MapWrapper
