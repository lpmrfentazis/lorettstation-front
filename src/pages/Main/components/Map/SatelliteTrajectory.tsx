import { Icon } from 'leaflet'
import { useEffect, useMemo, useRef, useState } from 'react'
import { Marker, Polyline, Tooltip } from 'react-leaflet'

import { useTrajectory } from '@/api/settings'

import { envConfig } from '@/config/constants'

import { useMapStore } from '@/store/mapStore'

import SatelliteIcon from '@/assets/images/satellite.png'

import { PlannedFullReq, TrajectoryPoint } from '@/types/schedule'

const findClosestTime = (arrayOfObjects: TrajectoryPoint[]) => {
  const currentTime = new Date()

  const nextObjectIdx =
    arrayOfObjects.findIndex(item => new Date(item.timeMoment) > currentTime) ||
    1
  return arrayOfObjects[nextObjectIdx - 1]
}

const satelliteIcon = new Icon({
  iconUrl: SatelliteIcon,
  iconSize: [40, 40],
})

export const SatelliteTrajectory = ({ satellite }: { satellite: string }) => {
  const trjectoryReq: PlannedFullReq = useMemo(() => {
    return {
      satellite: satellite,
      timeStart: new Date().toISOString(),
      timeEnd: new Date(
        Date.now() + envConfig.trajectoryTimeInterval,
      ).toISOString(),
      step: envConfig.trajectoryStep,
    }
  }, [satellite])

  const [curPos, setCurPos] = useState<TrajectoryPoint | null>(null)
  const [trajArray, setTrajArray] = useState<TrajectoryPoint[][]>([])

  const intervalRef = useRef<number>()
  const [isShownLine, setIsShownLine] = useMapStore(
    ({ shownSatellite, setShownSatellite }) => [
      shownSatellite,
      setShownSatellite,
    ],
  )

  const { trjectoryData } = useTrajectory(trjectoryReq)

  useEffect(() => {
    if (!trjectoryData) return
    const traj = trjectoryData.trajectory

    setCurPos(findClosestTime(traj))

    const newArray: TrajectoryPoint[][] = [[]]
    let curIndex = 0
    for (let i = 0; i < traj.length - 2; i++) {
      newArray[curIndex].push(traj[i])
      if (
        Math.abs(traj[i].lat - traj[i + 1].lat) > 90 ||
        Math.abs(traj[i].lng - traj[i + 1].lng) > 90
      ) {
        curIndex++
        newArray.push([])
      }
    }
    setTrajArray(newArray)

    intervalRef.current = setInterval(() => {
      setCurPos(findClosestTime(traj))
    }, 10000)

    return () => {
      clearInterval(intervalRef.current)
    }
  }, [trjectoryData])

  if (!trjectoryData) return null

  return (
    <>
      {isShownLine === satellite && (
        <>
          {trajArray.map((item, idx) => {
            return (
              <Polyline
                key={idx}
                positions={item}
              />
            )
          })}
        </>
      )}

      {curPos && (
        <Marker
          icon={satelliteIcon}
          position={curPos}
          eventHandlers={{
            click: () => setIsShownLine(satellite),
          }}
        >
          <Tooltip
            permanent
            direction="auto"
            offset={[20, 0]}
          >
            {satellite}
          </Tooltip>
        </Marker>
      )}
    </>
  )
}
