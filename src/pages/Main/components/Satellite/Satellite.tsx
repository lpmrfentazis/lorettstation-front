import {
  Box,
  Checkbox,
  Divider,
  Dropdown,
  Menu,
  MenuButton,
  MenuItem,
  Stack,
} from '@mui/joy'
import { ChevronDownIcon } from '@primer/octicons-react'
import { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'

import { useSatlistActive } from '@/api/settings'

import { useMapStore } from '@/store/mapStore'

export const Satellite = () => {
  const { t } = useTranslation()
  const { acitveData } = useSatlistActive()
  const [open, setOpen] = useState(false)

  const { activeSalist, toggleSatlistItem, setActiveSatlist } = useMapStore(
    ({ activeSalist, toggleSatlistItem, setActiveSatlist }) => ({
      activeSalist,
      toggleSatlistItem,
      setActiveSatlist,
    }),
  )

  useEffect(() => {
    if (!acitveData) return
    const saveAciveList: string[] = JSON.parse(
      localStorage.getItem('activeSatlist') || '[]',
    )
    setActiveSatlist(saveAciveList.filter(item => acitveData.includes(item)))
  }, [acitveData, setActiveSatlist])

  return (
    <Stack direction={'column'}>
      <Dropdown
        open={open}
        onOpenChange={() => setOpen(prev => !prev)}
      >
        <MenuButton
          endDecorator={
            <Box
              sx={{
                transform: `rotate(${open ? '180deg' : '0'})`,
                transition: 'transform 0.2s ease-in-out',
              }}
            >
              <ChevronDownIcon size={16} />
            </Box>
          }
        >
          {t('main.satellites')}
        </MenuButton>
        <Menu>
          <MenuItem
            slotProps={{
              root: {
                sx: { background: 'transparent !important' },
              },
            }}
            onClick={() => {
              setTimeout(() => {
                setOpen(true)
              }, 0)
            }}
          >
            <Stack
              divider={<Divider />}
              rowGap={'10px'}
              sx={{
                width: '100%',
                maxHeight: '400px',
                overflowX: 'hidden',
                overflowY: 'auto',
                py: '5px',
              }}
            >
              {acitveData &&
                acitveData.map((item, idx) => {
                  return (
                    <Checkbox
                      key={idx}
                      checked={activeSalist.includes(item)}
                      onChange={() => toggleSatlistItem(item)}
                      label={item}
                    />
                  )
                })}
            </Stack>
          </MenuItem>
        </Menu>
      </Dropdown>
    </Stack>
  )
}
