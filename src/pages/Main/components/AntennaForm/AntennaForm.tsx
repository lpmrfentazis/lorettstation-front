import { Input } from '@/widgets/Input/Input'
import { zodResolver } from '@hookform/resolvers/zod'
import { Button, Grid, Stack, Typography } from '@mui/joy'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'

import { useUpdateSettings } from '@/api/settings'

import { useMapStore } from '@/store/mapStore'

import { useCheckRights } from '@/hooks/checkRights'

import {
  AntennaFormSchema,
  LatLngSchema,
  TAntennaFormSchema,
} from '@/types/main'
import { TSettingsSchema } from '@/types/settings'

export const AntennaForm = ({
  settingsData,
}: {
  settingsData?: TSettingsSchema
}) => {
  const { t } = useTranslation()
  const hasAccessRights = useCheckRights(1)

  const { updateSettings, isPending } = useUpdateSettings()
  const formValues = AntennaFormSchema.optional().parse(settingsData)

  const { extraPos, setExtraPos } = useMapStore(
    ({ extraPos, setExtraPos }) => ({ extraPos, setExtraPos }),
  )

  const {
    register,
    handleSubmit,
    getValues,
    reset,
    formState: { dirtyFields, errors },
  } = useForm<TAntennaFormSchema>({
    resolver: zodResolver(AntennaFormSchema),
    values: formValues
      ? {
          stationName: formValues?.stationName,
          lat: extraPos?.lat || formValues?.lat,
          lon: extraPos?.lng || formValues?.lon,
          alt: formValues?.alt,
        }
      : formValues,
  })

  const onFieldBlur = () => {
    const latlng = LatLngSchema.safeParse(getValues())

    if (latlng.success) {
      setExtraPos(latlng.data)
    }
  }

  const submitForm = (data: TAntennaFormSchema) => {
    if (!settingsData) return
    updateSettings({ ...settingsData, ...data })
  }

  const onClear = () => {
    setExtraPos(null)
    reset()
  }

  return (
    <form onSubmit={handleSubmit(submitForm)}>
      <Stack>
        <Typography
          fontWeight={'bold'}
          sx={{ fontSize: '20px', mb: '16px' }}
        >
          {t('main.antenna')}
        </Typography>
        <Grid
          container
          rowSpacing={'15px'}
          columnSpacing={'15px'}
        >
          <Grid
            md={6}
            xs={12}
          >
            <Input
              label={t('main.inputs.title')}
              {...register('stationName')}
              errorLabel={errors.stationName?.message}
            />
          </Grid>
          <Grid
            md={6}
            xs={12}
          >
            <Input
              label={t('main.inputs.latitude')}
              {...register('lat', { valueAsNumber: true })}
              onBlur={onFieldBlur}
              errorLabel={errors.lat?.message}
            />
          </Grid>
          <Grid
            md={6}
            xs={12}
          >
            <Input
              label={t('main.inputs.longitude')}
              {...register('lon', { valueAsNumber: true })}
              onBlur={onFieldBlur}
              errorLabel={errors.lon?.message}
            />
          </Grid>
          <Grid
            md={6}
            xs={12}
          >
            <Input
              label={t('main.inputs.altitude')}
              {...register('alt', { valueAsNumber: true })}
              errorLabel={errors.alt?.message}
            />
          </Grid>

          {dirtyFields && (
            <>
              <Grid sm={6}>
                <Button
                  sx={{ width: '100%' }}
                  type="submit"
                  loading={isPending}
                  disabled={!hasAccessRights}
                >
                  {t('main.save')}
                </Button>
              </Grid>
              <Grid sm={6}>
                <Button
                  sx={{ width: '100%' }}
                  variant="outlined"
                  onClick={onClear}
                >
                  {t('main.cancel')}
                </Button>
              </Grid>
            </>
          )}
        </Grid>
      </Stack>
    </form>
  )
}
