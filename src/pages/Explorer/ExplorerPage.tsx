import { Wrapper } from '@/ui/Container/Wrapper'
import { Explorer } from '@/widgets/Explorer/Explorer'
import { Box, Typography } from '@mui/joy'
import { useTranslation } from 'react-i18next'

import { MainLayout } from '../../widgets/Layouts/MainLayout'

export const Component = () => {
  const { t } = useTranslation()

  return (
    <MainLayout>
      <Wrapper>
        <Box>
          <Typography
            level="h1"
            sx={{ mb: '20px' }}
          >
            {t('explorer.title')}
          </Typography>
          <Box
            sx={{
              height: 'calc(100vh - var(--Header-height) - 100px)',
            }}
          >
            <Explorer />
          </Box>
        </Box>
      </Wrapper>
    </MainLayout>
  )
}

Component.displayName = 'ExplorerPage'
