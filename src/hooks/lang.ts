import { useState } from 'react'
import { useTranslation } from 'react-i18next'

import { LangMode } from '@/types/main'

const getLang = (defaultLang: LangMode): LangMode => {
  const lang = localStorage.getItem('lang')

  if (!lang) {
    localStorage.setItem('lang', defaultLang)
    return defaultLang
  }

  return lang as LangMode
}

/**
 * Хук для работы с языком на сайте
 * @returns lang - текущий язык, setLang - функция для изменения языка
 */
export const useLanguage = () => {
  const { i18n } = useTranslation()
  const [lang, setLanguage] = useState<LangMode>(
    getLang(i18n.language.slice(0, 2) as LangMode),
  )

  const setLang = (mode: LangMode) => {
    setLanguage(mode)
    i18n.changeLanguage(mode)
    localStorage.setItem('lang', mode)
  }

  return { lang, setLang }
}
