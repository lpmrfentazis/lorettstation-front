import { useControlStore } from '@/store/controlStore'

export const useCheckRights = (needRole: number) => {
  const userRole = useControlStore(state => state.userRole)

  if (userRole !== null && userRole <= needRole) return true
  return false
}
