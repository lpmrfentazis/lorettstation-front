import { useCallback, useEffect, useState } from 'react'

/**
 * Хук позволяет отслеживать изменения размеров экрана
 * @returns \{ width, height \} - размеры экрана
 */
export const useDeviceSize = () => {
  const [width, setWidth] = useState(0)
  const [height, setHeight] = useState(0)

  const handleWindowResize = () => {
    setWidth(window.innerWidth)
    setHeight(window.innerHeight)
  }

  useEffect(() => {
    handleWindowResize()
    window.addEventListener('resize', handleWindowResize)
    return () => window.removeEventListener('resize', handleWindowResize)
  }, [])

  return { width, height }
}

/**
 * Хук позволяет отслеживать изменения размеров определенного html элемента
 * @param ref ссылка на элемент, за размерами которого мы следим
 * @returns dimentions - объект с размерами элементами
 */
export const useDimensions = (ref: React.MutableRefObject<Element | null>) => {
  const [dimensions, setDimensions] = useState<null | {
    width: number
    height: number
  }>(null)

  const handleWindowResize = useCallback(() => {
    if (!ref.current) return
    const { width, height } = ref.current.getBoundingClientRect()
    setDimensions({ width, height })
  }, [ref])

  useEffect(() => {
    handleWindowResize()
    window.addEventListener('resize', handleWindowResize)
    return () => window.removeEventListener('resize', handleWindowResize)
  }, [handleWindowResize])

  return dimensions
}
