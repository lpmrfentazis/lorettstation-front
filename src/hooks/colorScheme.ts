import { useColorScheme } from '@mui/joy'
import { useEffect, useState } from 'react'

import { ThemeMode } from '@/types/main'

const getTheme = (): ThemeMode => {
  return (localStorage.getItem('theme') as ThemeMode) || 'light'
}

/**
 * Хук позволяет получить текущую тему и поменять ее
 * @returns mode - текущий цвет, toggleColor - функция переключения
 */
export const useToggleColorScheme = () => {
  const [mode, setMode] = useState<ThemeMode>(getTheme())
  const { mode: muiMode, setMode: setMuiMode } = useColorScheme()

  const toggleColor = () => {
    if (mode === 'light') {
      setMuiMode('dark')
    } else {
      setMuiMode('light')
    }
  }

  useEffect(() => {
    if (!muiMode || muiMode === 'system') return
    setMode(muiMode)
  }, [muiMode])

  return { mode, toggleColor }
}

export type ThemeColors = {
  [key: string]: {
    dark: string
    light: string
  }
}

/**
 * Хук позволяет переключать цвета в зависимости от темы посредством изменения css переменных
 * @param colors ThemeColors - массив цветов для разных тем
 */
export const useThemedColors = (colors: ThemeColors) => {
  const { mode } = useToggleColorScheme()
  const root = document.documentElement

  useEffect(() => {
    Object.entries(colors).forEach(([key, value]) => {
      root.style.setProperty(key, value[mode])
    })
  }, [mode])
}
