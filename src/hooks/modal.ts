import { useCallback } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'

import { MODALS } from '@/config/routes'

import { AnyDict } from '@/types/main'

/**
 * Хук позволяет открывать модальные окна, а также прокидывать в них данные
 * @returns openModal - функция для открытия модалки
 */
export const useOpenModal = () => {
  const navigate = useNavigate()
  const location = useLocation()

  const openModal = useCallback(
    (modal: MODALS, meta?: AnyDict, shouldReplace?: boolean) => {
      const params = new URLSearchParams(location.search)
      params.set('modal', modal)
      return navigate(`${location.pathname}?${params.toString()}`, {
        state: meta,
        replace: shouldReplace,
      })
    },
    [location.pathname, location.search, navigate],
  )
  return { openModal }
}
