import { z } from 'zod'

import {
  AltitudeSchema,
  AzimuthCorrectionSchema,
  HorizonSchema,
  LatitudeSchema,
  LongitudeSchema,
  StationNameSchema,
} from './global'

export const SettingsSchema = z
  .object({
    alt: AltitudeSchema,
    azimuthCorrection: AzimuthCorrectionSchema,
    horizon: HorizonSchema,
    lat: LatitudeSchema,
    lon: LongitudeSchema,
    minApogee: HorizonSchema,
    stationName: StationNameSchema,
  })
  .refine(
    data => {
      if (data.minApogee >= data.horizon) {
        return true
      }
      return false
    },
    {
      path: ['minApogee'],
      params: {
        type: 'wrongHorizon',
      },
    },
  )
export type TSettingsSchema = z.infer<typeof SettingsSchema>

export const SatlistSchema = z.array(z.string())

export type TSatlistSchema = z.output<typeof SatlistSchema>
