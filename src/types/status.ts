import { z } from 'zod'

export const StatusResSchema = z
  .object({
    stationName: z.string(),
    battery: z.string().nullable(),
    status: z.string(),
    timemoment: z.string(),
  })
  .transform(data => {
    return {
      stationName: data.stationName,
      battery: data.battery,
      status: data.status,
      date: new Date(data.timemoment).getTime(),
    }
  })

export type TStatusResSchema = z.output<typeof StatusResSchema>
