import { z } from 'zod'

export const StationNameSchema = z.string().min(1).max(50)

export const LatitudeSchema = z.number().refine(
  data => {
    if (data >= -90 && data <= 90) return true
  },
  {
    params: {
      type: 'wrongRange',
      range: '[-90; 90]',
    },
  },
)

export const LongitudeSchema = z.number().refine(
  data => {
    if (data >= -180 && data <= 180) return true
  },
  {
    params: {
      type: 'wrongRange',
      range: '[-180; 180]',
    },
  },
)

export const AltitudeSchema = z.number().max(9000)

export const HorizonSchema = z.number().refine(
  data => {
    if (data >= 0 && data <= 90) return true
  },
  {
    params: {
      type: 'wrongRange',
      range: '[0; 90]',
    },
  },
)

export const AzimuthCorrectionSchema = z.number().refine(
  data => {
    if (data > -360 && data < 360) return true
  },
  {
    params: {
      type: 'wrongRange',
      range: '(-360; 360)',
    },
  },
)

export const AzimuthSchema = z.number().refine(
  data => {
    if (data >= 0 && data < 360) return true
  },
  {
    params: {
      type: 'wrongRange',
      range: '[0; 360)',
    },
  },
)

export const ElevationSchema = z.number().refine(
  data => {
    if (data >= 0 && data <= 90) return true
  },
  {
    params: {
      type: 'wrongRange',
      range: '[0; 90]',
    },
  },
)

export const DateTimeSchema = z
  .string()
  .transform(data => {
    return data[data.length - 1] === 'Z' ? data : `${data}Z`
  })
  .pipe(z.string().datetime())

export const ExplorerPathSchema = z.string()
