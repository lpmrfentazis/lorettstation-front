import { z } from 'zod'

export const TableTypeSchema = z.object({
  type: z.literal('table'),
  values: z.record(z.coerce.string()),
})

export const TelemetryResSchema = z.record(TableTypeSchema)

export type TableType = z.infer<typeof TableTypeSchema>
export type TelemetryRes = z.infer<typeof TelemetryResSchema>
