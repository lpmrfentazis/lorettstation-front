import { z } from 'zod'

import { DateTimeSchema } from './global'

export const ScheduleRes = {}

export enum FlyStatus {
  Waiting = 'wait',
  Collision = 'collision',
  Ignore = 'banned',
  Convoy = 'watching',
  Finished = 'finished',
}

export enum FlyType {
  Passed,
  Planned,
}

export const PassedFlySchema = z.object({
  passID: z.string(),
  passInfo: z.object({
    culmination: z.number(),
    orbit: z.number(),
    timeEnd: z.string(),
    timeStart: z.string(),
  }),
  satellite: z.string(),
  stationInfo: z.object({
    alt: z.number(),
    lat: z.number(),
    lon: z.number(),
    name: z.string(),
  }),
})

export type TPassedFlySchema = z.infer<typeof PassedFlySchema>

export const RecordInfoLogSchema = z.object({
  azimuth: z.number(),
  elevation: z.number(),
  level: z.number(),
  snr: z.number(),
  timeMoment: z.string(),
})

export type TRecordInfoLogSchema = z.infer<typeof RecordInfoLogSchema>

export const RecordInfoSchema = z.object({
  frequency: z.number(),
  frequencyOffset: z.number(),
  gain: z.number(),
  log: z.array(RecordInfoLogSchema),
})

export const PassedFlyFullSchema = PassedFlySchema.extend({
  recordInfo: RecordInfoSchema,
})

export type TPassedFlyFullSchema = z.infer<typeof PassedFlyFullSchema>

export const PlannedFlySchema = z.object({
  satName: z.string(),
  culmination: z.number(),
  trueCulmination: z.number(),
  timeStart: z.string(),
  timeEnd: z.string(),
  timeCulmination: z.string(),
  trueTimeCulmination: z.string(),
  trueTimeStart: z.string(),
  trueTimeEnd: z.string(),
  orbit: z.number(),
  status: z.nativeEnum(FlyStatus),
})

export type TPlannedFlySchema = z.infer<typeof PlannedFlySchema>

export const PlannedFlyPointSchema = z.object({
  azimuth: z.number(),
  elevation: z.number(),
  timeMoment: z.string(),
})

export const PlannedFlyFullSchema = z.object({
  satellite: z.string(),
  timeEnd: z.string(),
  timeStart: z.string(),
  track: z.array(PlannedFlyPointSchema),
})
export type TPlannedFlyPointSchema = z.infer<typeof PlannedFlyPointSchema>

export type TPlannedFlyFullSchema = z.infer<typeof PlannedFlyFullSchema>

export const TrajectoryPointSchema = z
  .object({
    alt: z.number(),
    lat: z.number(),
    lon: z.number(),
    timeMoment: z.string(),
  })
  .transform(data => {
    return {
      lat: data.lat,
      lng: data.lon,
      timeMoment: data.timeMoment,
    }
  })

export type TrajectoryPoint = z.infer<typeof TrajectoryPointSchema>

export const PlannedFlyTrajectorySchema = z.object({
  satellite: z.string(),
  timeEnd: z.string(),
  timeStart: z.string(),
  trajectory: z.array(TrajectoryPointSchema),
})

export const LastUpdateSchema = DateTimeSchema

export const PlannedFullReqSchema = z
  .object({
    satName: z.string().default(''),
    trueTimeStart: z.string().default(''),
    trueTimeEnd: z.string().default(''),
    step: z.string().default('1'),
  })
  .transform(data => {
    return {
      satellite: data?.satName,
      timeStart: data?.trueTimeStart,
      timeEnd: data?.trueTimeEnd,
      step: data?.step,
    }
  })

export type PlannedFullReq = z.infer<typeof PlannedFullReqSchema>
