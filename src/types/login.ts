import { z } from 'zod'

export const UserSchema = z.object({
  active: z.boolean(),
  name: z.string(),
  role: z.string(),
  roleID: z.number(),
  username: z.string(),
})

export type TUserSchema = z.infer<typeof UserSchema>

export const LoginSchema = z.object({
  username: z.string(),
  password: z.string(),
})

export type TLoginSchema = z.infer<typeof LoginSchema>

export const LoginRespSchema = z.object({
  token: z.string().min(1),
})
export type TLoginRespSchema = z.infer<typeof LoginRespSchema>
