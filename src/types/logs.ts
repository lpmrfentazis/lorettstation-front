import { z } from 'zod'

export enum LogStatus {
  Info = 'INFO',
  Error = 'ERROR',
  Warning = 'WARNING',
}

export const LogSchema = z.object({
  timeMoment: z.string(),
  level: z.string(),
  message: z.string(),
})

export type TLogSchema = z.infer<typeof LogSchema>
