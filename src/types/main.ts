import { z } from 'zod'

import {
  AltitudeSchema,
  LatitudeSchema,
  LongitudeSchema,
  StationNameSchema,
} from './global'

export type ModalProps = {
  isOpen: boolean
}

export type AnyDict = {
  [key: string]: unknown
}

export const LatLngSchema = z
  .object({
    lat: LatitudeSchema,
    lon: LongitudeSchema,
  })
  .transform(data => {
    return {
      lat: data.lat,
      lng: data.lon,
    }
  })

export type LatLng = {
  lat: number
  lng: number
}

export type ThemeMode = 'light' | 'dark'
export type LangMode = 'ru' | 'en'

export const AntennaFormSchema = z.object({
  stationName: StationNameSchema,
  lat: LatitudeSchema,
  lon: LongitudeSchema,
  alt: AltitudeSchema,
})

export type TAntennaFormSchema = z.infer<typeof AntennaFormSchema>

export const MapSchema = z
  .object({
    stationName: StationNameSchema,
    lat: LatitudeSchema,
    lon: LongitudeSchema,
  })
  .transform(data => {
    return {
      stationName: data.stationName,
      stationPos: { lat: data.lat, lng: data.lon },
    }
  })

export type TMapSchema = z.infer<typeof MapSchema>
