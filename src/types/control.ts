import { z } from 'zod'

import { AzimuthSchema, ElevationSchema } from './global'
import { ThemeMode } from './main'

export const AzimuthResSchema = z.object({
  azimuth: z.number().nullable(),
  elevation: z.number().nullable(),
})

export type TAzimuthResSchema = z.infer<typeof AzimuthResSchema>

export type AzimuthData = {
  azimuth: number
  elevation: number
}

export type AzimuthContainerProps = AzimuthData & {
  mode: ThemeMode
}
export const ControlSchema = z.object({
  azimuth: AzimuthSchema,
  elevation: ElevationSchema,
})

export type TControlSchema = z.infer<typeof ControlSchema>
