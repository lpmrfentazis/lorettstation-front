import axios, { AxiosRequestConfig } from 'axios'
import { ZodSchema, ZodTypeDef } from 'zod'

import { envConfig } from '@/config/constants'

import { getAccessToken } from '@/utils/login'

interface BaseApiRequestOptions {
  url: string
  method?: 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH'
  data?: object
  params?: AxiosRequestConfig
}

export class BaseService {
  protected async apiRequest<T>({
    url,
    method,
    data,
    params,
  }: BaseApiRequestOptions): Promise<T> {
    const signature = `Bearer ${getAccessToken()}`

    const headers: Record<string, string> = {
      Authorization: signature,
      ['ngrok-skip-browser-warning']: 'true',
    }

    const apiUrl = `${envConfig.apiUrl}${url}`
    const response = await axios({
      method,
      url: apiUrl,
      headers,
      data,
      ...params,
    })

    return response.data
  }

  protected validateData<T>(
    data: unknown,
    schema: ZodSchema<T, ZodTypeDef, unknown>,
  ): T {
    const validData = schema.safeParse(data)
    if (validData.success) {
      return validData.data
    } else {
      console.error(validData.error)
      throw validData.error
    }
  }
}
