import { ExplorerPathSchema } from '@/types/global'
import { PassedFlyFullSchema, PassedFlySchema } from '@/types/schedule'

import { BaseService } from './baseService'

class RecordsService extends BaseService {
  private baseUrl = '/api/v1/records'

  async getPassedSchedule(limit: number) {
    const res = await this.apiRequest({
      url: `${this.baseUrl}/logs?limit=${limit}`,
    })
    return this.validateData(res, PassedFlySchema.array())
  }

  async getPassedScheduleFull(passID: string) {
    const res = await this.apiRequest({ url: `${this.baseUrl}/logs/${passID}` })
    return this.validateData(res, PassedFlyFullSchema)
  }

  async getExplorerPath(passID: string) {
    const res = await this.apiRequest({
      url: `${this.baseUrl}/data/${passID}/explorer/path`,
    })
    return this.validateData(res, ExplorerPathSchema)
  }
}

export default new RecordsService()
