import { AxiosProgressEvent } from 'axios'

import { downloadFile } from '@/utils/downloadFile'

import { LogSchema } from '@/types/logs'
import {
  SatlistSchema,
  SettingsSchema,
  TSettingsSchema,
} from '@/types/settings'
import { StatusResSchema } from '@/types/status'
import { TelemetryResSchema } from '@/types/telemetry'

import { BaseService } from './baseService'

class TelemetryService extends BaseService {
  private baseUrl = '/api/v1'

  async getSummary() {
    const res = await this.apiRequest({
      url: `${this.baseUrl}/telemetry/summary`,
    })
    return this.validateData(res, TelemetryResSchema)
  }

  async getStatus() {
    const res = await this.apiRequest({
      url: `${this.baseUrl}/station/telemetry/status`,
    })
    return this.validateData(res, StatusResSchema)
  }

  async getSettings() {
    const res = await this.apiRequest({
      url: `${this.baseUrl}/station/telemetry/settings`,
    })
    return this.validateData(res, SettingsSchema)
  }

  async putSettings(data: TSettingsSchema) {
    await this.apiRequest({
      method: 'PUT',
      url: `${this.baseUrl}/station/telemetry/settings`,
      data,
    })
  }

  async getLogs({
    offset = 0,
    limit = 20,
  }: {
    offset?: number
    limit?: number
  }) {
    const res = await this.apiRequest({
      url: `${this.baseUrl}/telemetry/logs?offset=${offset}&limit=${limit}`,
    })
    return this.validateData(res, LogSchema.array())
  }

  async getLogsFile(
    onDownloadProgress?: (progress: AxiosProgressEvent) => void,
  ) {
    const res = await this.apiRequest<Blob>({
      url: `${this.baseUrl}/telemetry/logs/file`,
      params: {
        responseType: 'blob',
        onDownloadProgress: onDownloadProgress,
      },
    })
    return downloadFile(res)
  }

  async getFullSatlist() {
    const res = await this.apiRequest({
      url: `${this.baseUrl}/station/telemetry/settings/satlist/full`,
    })
    return this.validateData(res, SatlistSchema)
  }

  async getActiveSatlist() {
    const res = await this.apiRequest({
      url: `${this.baseUrl}/station/telemetry/settings/satlist/active`,
    })
    return this.validateData(res, SatlistSchema)
  }

  async putActiveSatlist(data: string[]) {
    return this.apiRequest({
      method: 'PUT',
      url: `${this.baseUrl}/station/telemetry/settings/satlist/active`,
      data,
    })
  }
}

export default new TelemetryService()
