import {
  LastUpdateSchema,
  PlannedFlyFullSchema,
  PlannedFlySchema,
  PlannedFlyTrajectorySchema,
  TPlannedFlySchema,
} from '@/types/schedule'

import { BaseService } from './baseService'

class OrbitalsService extends BaseService {
  private baseUrl = '/api/v1/orbitals'

  async getLastUpdateSchedule() {
    const res = await this.apiRequest({
      url: `${this.baseUrl}/satellites/schedule/lastUpdate`,
    })
    return this.validateData(res, LastUpdateSchema)
  }

  async getPlannedSchedule(limit: number) {
    const res = await this.apiRequest({
      url: `${this.baseUrl}/satellites/schedule?limit=${limit}`,
    })
    return this.validateData(res, PlannedFlySchema.array())
  }

  async putPlannedSchedule(data: TPlannedFlySchema) {
    await this.apiRequest({
      url: `${this.baseUrl}/satellites/schedule`,
      method: 'PUT',
      data,
    })
  }

  async getPlannedScheduleFull(reqParams: string) {
    const res = await this.apiRequest({
      url: `${this.baseUrl}/satellites/track?${reqParams}`,
    })
    return this.validateData(res, PlannedFlyFullSchema)
  }

  async getPlannedTrajectory(reqParams: string) {
    const res = await this.apiRequest({
      url: `${this.baseUrl}/satellites/trajectory?${reqParams}`,
    })
    return this.validateData(res, PlannedFlyTrajectorySchema)
  }
}

export default new OrbitalsService()
