import { LoginRespSchema, TLoginSchema, UserSchema } from '@/types/login'

import { BaseService } from './baseService'

class LoginService extends BaseService {
  async login(data: TLoginSchema) {
    const res = await this.apiRequest({
      url: '/jwt/login',
      method: 'POST',
      data,
    })
    return this.validateData(res, LoginRespSchema)
  }

  async getUser() {
    const res = await this.apiRequest({
      url: '/jwt/user',
    })
    return this.validateData(res, UserSchema)
  }
}

export default new LoginService()
