import { AzimuthResSchema, TControlSchema } from '@/types/control'

import { BaseService } from './baseService'

class RotatorService extends BaseService {
  private baseUrl = `/api/v1/rotator`

  async getControl() {
    const res = await this.apiRequest({
      url: `${this.baseUrl}/telemetry/orientation`,
    })
    return this.validateData(res, AzimuthResSchema)
  }

  async relativeMove(data: TControlSchema) {
    return this.apiRequest({
      method: 'POST',
      url: `${this.baseUrl}/controler/movements/relative`,
      data,
    })
  }

  async absoluteMove(data: TControlSchema) {
    return this.apiRequest({
      method: 'POST',
      url: `${this.baseUrl}/controler/movements/absolute`,
      data,
    })
  }

  async stop() {
    return this.apiRequest({
      method: 'POST',
      url: `${this.baseUrl}/controler/movements/stop`,
    })
  }

  async calibrate() {
    return this.apiRequest({
      method: 'POST',
      url: `${this.baseUrl}/controler/movements/calibrate`,
    })
  }

  async startPosition() {
    return this.apiRequest({
      method: 'POST',
      url: `${this.baseUrl}/controler/movements/start_position`,
    })
  }

  async saveLandmark(data: { value: number }) {
    return this.apiRequest({
      method: 'POST',
      url: `${this.baseUrl}/controler/movements/save_landmark`,
      data,
    })
  }
}

export default new RotatorService()
