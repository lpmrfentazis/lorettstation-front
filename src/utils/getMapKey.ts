import { envConfig } from '@/config/constants'

export const getMapKey = () => {
  if (envConfig.isDev) return ''
  return envConfig.apiMapKey ? '?api_key=' + envConfig.apiMapKey : ''
}
