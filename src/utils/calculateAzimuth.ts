export const toRadians = (deg: number) => deg * (Math.PI / 180)
export const toDegrees = (rad: number) => rad * (180 / Math.PI)

export function calculate(
  lat1: number,
  lon1: number,
  lat2: number,
  lon2: number,
) {
  // Eccentricity of the spheroid
  const a = 6378137 // Semi-major axis for WGS 84
  const e = 0.08181919092890624

  // Convert input coordinates to radians
  lat1 = toRadians(lat1)
  lon1 = toRadians(lon1)
  lat2 = toRadians(lat2)
  lon2 = toRadians(lon2)

  // Calculate Δλ
  let deltaLambda = toDegrees(lon2 - lon1)
  if (deltaLambda > 180) deltaLambda -= 360
  else if (deltaLambda < -180) deltaLambda += 360

  // Calculate the course angle α
  let alpha = Math.atan2(
    toRadians(deltaLambda),

    Math.log(
      Math.tan(Math.PI / 4 + lat2 / 2) *
        Math.pow((1 - e * Math.sin(lat2)) / (1 + e * Math.sin(lat2)), e / 2),
    ) -
      Math.log(
        Math.tan(Math.PI / 4 + lat1 / 2) *
          Math.pow((1 - e * Math.sin(lat1)) / (1 + e * Math.sin(lat1)), e / 2),
      ),
  )

  if (alpha < 0) {
    alpha = Math.PI * 2 + alpha
  }

  const S = Math.abs(
    a *
      (1 / Math.cos(alpha)) *
      ((1 - (e * e) / 4) * Math.abs(lat2 - lat1) -
        (1 / 8) * e * e * (Math.sin(2 * lat2) - Math.sin(2 * lat1))),
  )

  return {
    alpha: toDegrees(alpha),
    S: S,
  }
}
