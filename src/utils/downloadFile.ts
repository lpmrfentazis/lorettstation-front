export const downloadFile = (resp: Blob) => {
  const href = URL.createObjectURL(resp)
  const link = document.createElement('a')
  link.href = href
  link.setAttribute('download', 'file.log') //or any other extension
  document.body.appendChild(link)
  link.click()
  document.body.removeChild(link)
  URL.revokeObjectURL(href)
}
