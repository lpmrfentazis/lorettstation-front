import { TIME_ZONE_OFFSET } from '@/config/constants'

export enum TIME_ZONE {
  UTC,
  Our,
}

export const getDate = (
  timeStamp: number,
  timeZone: TIME_ZONE = TIME_ZONE.UTC,
) => {
  const curTimeStamp =
    timeZone === TIME_ZONE.Our ? timeStamp - TIME_ZONE_OFFSET : timeStamp
  const date = new Date(curTimeStamp).toLocaleDateString('ru')
  return date
}

export const getTime = (
  timeStamp: number,
  timeZone: TIME_ZONE = TIME_ZONE.UTC,
) => {
  const curTimeStamp =
    timeZone === TIME_ZONE.Our ? timeStamp - TIME_ZONE_OFFSET : timeStamp
  const date = new Date(curTimeStamp).toLocaleTimeString('ru')
  return date
}
