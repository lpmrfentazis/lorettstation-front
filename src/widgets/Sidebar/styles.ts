export const SidebarStyle = {
  position: { xs: 'fixed', md: 'sticky' },
  transform: {
    xs: 'translateX(calc(100% * (var(--SideNavigation-slideIn, 0) - 1)))',
    md: 'none',
  },
  transition: 'transform 0.4s, width 0.4s',
  zIndex: 100,
  height: '100dvh',
  width: 'var(--Sidebar-width)',
  top: 0,
  p: 2,
  flexShrink: 0,
  display: 'flex',
  flexDirection: 'column',
  gap: 2,
  borderRight: '1px solid',
  borderColor: 'divider',
}

export const SidebarOverlayStyle = {
  position: 'fixed',
  zIndex: 98,
  top: 0,
  left: 0,
  width: '100vw',
  height: '100vh',
  opacity: 'var(--SideNavigation-slideIn)',
  backgroundColor: 'var(--joy-palette-background-backdrop)',
  transition: 'opacity 0.4s',
  transform: {
    xs: 'translateX(calc(100% * (var(--SideNavigation-slideIn, 0) - 1) + var(--SideNavigation-slideIn, 0) * var(--Sidebar-width, 0px)))',
    lg: 'translateX(-100%)',
  },
}
