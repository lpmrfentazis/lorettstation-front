import {
  Box,
  Divider,
  GlobalStyles,
  IconButton,
  Link,
  Sheet,
  Skeleton,
  Stack,
  Typography,
} from '@mui/joy'
import { MoonIcon, SignOutIcon, SunIcon } from '@primer/octicons-react'
import { useQueryClient } from '@tanstack/react-query'
import { useTranslation } from 'react-i18next'

import { useControlToStore } from '@/api/control'
import { useUser } from '@/api/login'
import { useStatus } from '@/api/status'

import { MODALS } from '@/config/routes'

import { useToggleColorScheme } from '@/hooks/colorScheme'
import { useOpenModal } from '@/hooks/modal'

import { clearAccessToken } from '@/utils/login'
import { closeSidebar } from '@/utils/sideabar'

import LogoImageWhite from '@/assets/lorett-white.svg'
import LogoImage from '@/assets/lorett.svg'

import { RouteList } from '../RouteList/RouteList'
import { StatusBar } from '../StatusBar/StatusBar'
import { SidebarOverlayStyle, SidebarStyle } from './styles'

export const Sidebar = () => {
  const { mode, toggleColor } = useToggleColorScheme()
  const { userData, isUserLoading, isUserError } = useUser()
  const { openModal } = useOpenModal()
  const queryClient = useQueryClient()
  const { t } = useTranslation()

  const onExitClick = () => {
    clearAccessToken()
    queryClient.invalidateQueries({ queryKey: ['user'] })
  }

  const { statusData } = useStatus()
  useControlToStore()

  return (
    <Sheet
      className="Sidebar"
      sx={SidebarStyle}
    >
      <GlobalStyles
        styles={theme => ({
          ':root': {
            '--Sidebar-width': '280px',
            [theme.breakpoints.up('lg')]: {
              '--Sidebar-width': '330px',
            },
          },
        })}
      />
      <Box
        className="Sidebar-overlay"
        sx={SidebarOverlayStyle}
        onClick={() => closeSidebar()}
      />
      <Stack
        direction={'column'}
        height={'100%'}
      >
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            flexShrink: 0,
            flexGrow: 1,
          }}
        >
          <Box sx={{ mb: { xs: '25px', lg: '49px' } }}>
            <Stack
              direction={'row'}
              justifyContent={'space-between'}
            >
              <img
                src={mode === 'light' ? LogoImage : LogoImageWhite}
                width={120}
                height={85}
                alt="logo"
              />
              <Stack
                alignItems={'center'}
                justifyContent={'center'}
              >
                <IconButton
                  size="sm"
                  variant="outlined"
                  color="neutral"
                  onClick={toggleColor}
                  sx={{ borderRadius: '100%' }}
                >
                  {mode === 'light' ? (
                    <MoonIcon size={18} />
                  ) : (
                    <SunIcon size={18} />
                  )}
                </IconButton>
              </Stack>
            </Stack>
            <RouteList />
          </Box>

          <Stack gap={'12px'}>
            <StatusBar status={statusData} />
          </Stack>
        </Box>
        <Divider />
        <Box sx={{ display: 'flex', gap: 1, alignItems: 'center', py: '5px' }}>
          <Box sx={{ minWidth: 0, flex: 1 }}>
            {isUserLoading || isUserError ? (
              <Skeleton
                animation="wave"
                variant="inline"
                loading={isUserLoading}
              >
                <Link onClick={() => openModal(MODALS.Login)}>
                  {t('sidebar.login')}
                </Link>
              </Skeleton>
            ) : (
              <Stack>
                <Typography fontWeight={'bold'}>
                  {userData?.username}
                </Typography>
                <Typography level="body-xs">{userData?.role}</Typography>
              </Stack>
            )}
          </Box>
          <IconButton
            size="sm"
            variant="plain"
            color="neutral"
            onClick={onExitClick}
          >
            <SignOutIcon size={16} />
          </IconButton>
        </Box>
      </Stack>
    </Sheet>
  )
}
