import { Stack, Typography } from '@mui/joy'
import { AlertFillIcon } from '@primer/octicons-react'

export const InputError = ({ text }: { text: string }) => {
  return (
    <Stack
      direction={'row'}
      alignItems={'center'}
      columnGap={'5px'}
      sx={{ p: '5px' }}
    >
      <AlertFillIcon
        size={14}
        fill="#e47474"
      />
      <Typography
        fontSize={'14px'}
        color="danger"
      >
        {text}
      </Typography>
    </Stack>
  )
}
