import { GlobalStyles, IconButton, Sheet } from '@mui/joy'
import { ThreeBarsIcon } from '@primer/octicons-react'

import { toggleSidebar } from '@/utils/sideabar'

export const Header = () => {
  return (
    <Sheet
      sx={{
        display: { xs: 'flex', md: 'none' },
        alignItems: 'center',
        justifyContent: 'space-between',
        position: 'fixed',
        top: 0,
        width: '100vw',
        height: 'var(--Header-height)',

        zIndex: 99,
        p: 2,
        gap: 1,
        borderBottom: '1px solid',
        borderColor: 'background.level1',
        boxShadow: 'sm',
      }}
    >
      <GlobalStyles
        styles={theme => ({
          ':root': {
            '--Header-height': '52px',
            [theme.breakpoints.up('md')]: {
              '--Header-height': '0px',
            },
          },
        })}
      />
      <IconButton
        variant="outlined"
        onClick={() => toggleSidebar()}
        color="neutral"
        size="sm"
      >
        {/* <MenuIcon /> */}
        <ThreeBarsIcon size={16} />
      </IconButton>
    </Sheet>
  )
}
