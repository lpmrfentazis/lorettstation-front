import { Button, Stack, Typography } from '@mui/joy'
import { useQueryClient } from '@tanstack/react-query'
import { useEffect } from 'react'
import { useTranslation } from 'react-i18next'

import { useExplorer } from '@/api/explorer'

import { envConfig } from '@/config/constants'
import { MODALS } from '@/config/routes'

import { useOpenModal } from '@/hooks/modal'

import '@/styles/explorer.css'

import { Loader } from '../Loader/Loader'

export const Explorer = ({
  path = '',
  inlineModal = false,
}: {
  path?: string
  inlineModal?: boolean
}) => {
  const { isLoading, isError } = useExplorer()
  const { openModal } = useOpenModal()
  const { t } = useTranslation()
  const queryClient = useQueryClient()

  const onUpdate = () => {
    queryClient.invalidateQueries({ queryKey: ['explorer'] })
  }

  useEffect(() => {
    if (isError && !isLoading && !inlineModal)
      openModal(MODALS.UnavailableExplorer, {}, true)
  }, [isLoading, isError, inlineModal, openModal])

  return (
    <>
      {isLoading && !inlineModal && <Loader />}

      {inlineModal && !isLoading && isError && (
        <Stack
          justifyContent={'center'}
          alignItems={'center'}
          height={'100%'}
        >
          <Stack
            gap={'15px'}
            sx={{ maxWidth: '400px' }}
          >
            <Typography>{t('modals.unavailableExplorer.text')}</Typography>
            <Button
              variant="outlined"
              color="danger"
              onClick={onUpdate}
              loading={isLoading}
            >
              {t('modals.unavailableExplorer.update')}
            </Button>
          </Stack>
        </Stack>
      )}

      {!isError && !isLoading && (
        <iframe
          width={'100%'}
          height={'100%'}
          src={`${envConfig.explorerUrl}${path}`}
        />
      )}
    </>
  )
}
