import { GlobalContainer } from '@/ui/Container/GlobalContainer'
import { Modals } from '@/widgets/Modals/Modals'
import { CssVarsProvider, StyledEngineProvider } from '@mui/joy'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import { ReactQueryDevtools } from '@tanstack/react-query-devtools'
import { Outlet } from 'react-router-dom'
import { Toaster } from 'sonner'

import { theme } from '@/config/theme'

import { useLanguage } from '@/hooks/lang'

import '@/utils/i18n'

import { Head } from '../Head/Head'

const queryClient = new QueryClient({
  defaultOptions: {
    queries: { refetchOnWindowFocus: true },
  },
})

export const App = () => {
  useLanguage()

  return (
    <StyledEngineProvider injectFirst>
      <CssVarsProvider theme={theme}>
        <QueryClientProvider client={queryClient}>
          <Head />
          <GlobalContainer>
            <Outlet />
            <Toaster richColors />
            <Modals />
            <ReactQueryDevtools initialIsOpen={false} />
          </GlobalContainer>
        </QueryClientProvider>
      </CssVarsProvider>
    </StyledEngineProvider>
  )
}
