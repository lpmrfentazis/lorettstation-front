import { Box, Link, Sheet, Table } from '@mui/joy'
import { TriangleDownIcon } from '@primer/octicons-react'
import React from 'react'

import { TableType } from '@/types/telemetry'

const CollapsibleTable = ({
  title,
  table,
}: {
  title: string
  table: TableType
}) => {
  const [expanded, setExpanded] = React.useState(true)

  return (
    <Box>
      <Sheet
        variant="plain"
        sx={{
          borderRadius: 'sm',
        }}
      >
        <Table
          stickyHeader
          hoverRow
          borderAxis="both"
          sx={{
            '--TableCell-headBackground':
              'var(--joy-palette-background-level1)',
            '--Table-headerUnderlineThickness': '1px',
            '--TableRow-hoverBackground':
              'var(--joy-palette-background-level1)',
            '--TableCell-paddingY': '4px',
            '--TableCell-paddingX': '8px',
          }}
        >
          <thead>
            <tr>
              <th>
                <Link
                  underline="none"
                  component="button"
                  color="neutral"
                  onClick={() => setExpanded(prev => !prev)}
                  fontWeight="lg"
                  endDecorator={<TriangleDownIcon size={16} />}
                  sx={{
                    fontSize: '1rem',
                    '& svg': {
                      transition: '0.2s',
                      transform: expanded ? 'rotate(180deg)' : 'rotate(0deg)',
                    },
                  }}
                >
                  {title}
                </Link>
              </th>
              <th></th>
            </tr>
          </thead>
          {expanded && (
            <tbody>
              {Object.entries(table.values).map(([key, value]) => {
                return (
                  <tr key={key}>
                    <td style={{ fontWeight: 'bold' }}>{key}</td>
                    <td>{value.toString()}</td>
                  </tr>
                )
              })}
            </tbody>
          )}
        </Table>
      </Sheet>
    </Box>
  )
}

export default CollapsibleTable
