import * as PIXI from 'pixijs'
import { memo, useEffect, useRef } from 'react'

import { useDimensions } from '@/hooks/size'

export const CanvasContainer = memo(
  ({
    widget,
    width,
    height,
  }: {
    widget: PIXI.Container | null
    width: number
    height: number
  }) => {
    const pixiContainerRef = useRef<HTMLDivElement>(null)
    const widgetDimensions = useDimensions(pixiContainerRef)

    useEffect(() => {
      if (!pixiContainerRef.current || !widgetDimensions || !widget) return
      const scaleCoef = widgetDimensions.width / width

      const app = new PIXI.Application({
        width: widgetDimensions.width,
        height: height * scaleCoef,
        backgroundAlpha: 0,
        resolution: window.devicePixelRatio,
        antialias: true,
        autoDensity: true,
      })

      app.stage.sortableChildren = true

      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      pixiContainerRef.current.appendChild(app.view)

      widget?.scale?.set(scaleCoef)
      app.stage.addChild(widget)

      return () => {
        app.destroy(true)
      }
    }, [widgetDimensions, width, height, widget, pixiContainerRef.current])

    return (
      <div
        ref={pixiContainerRef}
        // style={{ height: '100%' }}
        // style={{ overflow: 'auto' }}
      />
    )
  },
)
