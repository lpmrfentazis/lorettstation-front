import { Header } from '@/widgets/Header/Header'
import { Sidebar } from '@/widgets/Sidebar/Sidebar'
import { Box, Sheet } from '@mui/joy'

export const MainLayout = ({ children }: { children: React.ReactNode }) => {
  return (
    <Box sx={{ display: 'flex', minHeight: '100vh' }}>
      <Header />
      <Sidebar />
      <Sheet
        component="main"
        sx={{
          width: '100%',
          // minHeight: '100dvh',
          py: {
            xs: 'calc(12px + var(--Header-height))',
            sm: 'calc(12px + var(--Header-height))',
            md: 'calc(12px + var(--Header-height))',
          },
        }}
      >
        {children}
      </Sheet>
    </Box>
  )
}
