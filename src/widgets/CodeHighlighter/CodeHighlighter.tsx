import hljs from 'highlight.js/lib/core'
import json from 'highlight.js/lib/languages/json'
import { memo } from 'react'

hljs.registerLanguage('json', json)

export const CodeHighlighter = memo(({ code }: { code: string }) => {
  const formattedCode = hljs.highlight(code, { language: 'json' }).value

  return (
    <pre style={{ overflowX: 'auto' }}>
      <code
        style={{ fontFamily: 'Roboto Mono' }}
        dangerouslySetInnerHTML={{ __html: formattedCode }}
      ></code>
    </pre>
  )
})
