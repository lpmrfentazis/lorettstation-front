export const colors = {
  '--hljs-color': {
    light: '#2a2c2d',
    dark: '#e6e6e6',
  },
  '--hljs-bgcolor': {
    light: '#e6e6e6',
    dark: '#2a2c2d',
  },
  '--hljs-quote-color': {
    light: '#676b79',
    dark: '#bbbbbb',
  },
  '--hljs-comment-color': {
    light: '#676b79',
    dark: '#bbbbbb',
  },
  '--hljs-punctuation-color': {
    light: '#2a2c2d',
    dark: '#e6e6e6',
  },
  '--hljs-selector-tag-color': {
    light: '#c56200',
    dark: '#ff4b82',
  },
  '--hljs-operator-color': {
    light: '',
    dark: '#b084eb',
  },
  '--hljs-keyword-color': {
    light: '#d92792',
    dark: '#ff75b5',
  },
  '--hljs-regexp-color': {
    light: '#cc5e91',
    dark: '#ff9ac1',
  },
  '--hljs-subst-color': {
    light: '#3787c7',
    dark: '#45a9f9',
  },
  '--hljs-string-color': {
    light: '#0d7d6c',
    dark: '#19f9d8',
  },
  '--hljs-variable-color': {
    light: '#7641bb',
    dark: '#ffb86c',
  },
}
