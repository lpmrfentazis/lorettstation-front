import {
  Box,
  InputProps as DefaultInputProps,
  Input as MUIInput,
  Typography,
  TypographySystem,
} from '@mui/joy'
import { forwardRef } from 'react'

import { InputError } from '../InputError/InputError'

type InputProps = {
  label?: string
  labelLavel?: keyof TypographySystem | 'inherit'
  errorLabel?: string
  tabIndex?: number
} & DefaultInputProps

export const Input = forwardRef<HTMLInputElement, InputProps>(
  ({ label, labelLavel = 'inherit', errorLabel, tabIndex, ...props }, ref) => {
    return (
      <Box>
        {label && (
          <Typography
            level={labelLavel}
            fontWeight={'bold'}
            sx={{ mb: '5px' }}
          >
            {label}
          </Typography>
        )}
        <MUIInput
          sx={{ width: '100%', background: 'transparent', boxShadow: 'none' }}
          ref={ref}
          slotProps={{
            input: {
              tabIndex,
            },
          }}
          {...props}
        />
        {errorLabel && <InputError text={errorLabel} />}
      </Box>
    )
  },
)
