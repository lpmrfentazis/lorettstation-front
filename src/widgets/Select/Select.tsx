import { Box, Select as MUISelect, Option, Typography } from '@mui/joy'
import { useTranslation } from 'react-i18next'

type SelectProps<T extends string> = {
  label?: string
  values: SelectValues<T>
  currentValue: T
  onChange?: (s: T) => void
}

type SelectValueType = {
  i18n?: string
  text?: string
}

type SelectValues<T extends string> = {
  [key in T]: SelectValueType
}

export function Select<T extends string>({
  label,
  values,
  onChange,
  currentValue,
}: SelectProps<T>) {
  const { t } = useTranslation()

  return (
    <Box>
      {label && (
        <Typography
          fontWeight={'bold'}
          sx={{ mb: '5px' }}
        >
          {label}
        </Typography>
      )}
      <MUISelect
        sx={{ background: 'transparent', boxShadow: 'none' }}
        value={currentValue}
        onChange={(_, newValue) => {
          newValue && onChange && onChange(newValue)
        }}
      >
        {values &&
          Object.entries<SelectValueType>(values).map(([key, value]) => {
            return (
              <Option
                key={key}
                value={key}
              >
                {value.i18n ? t(value.i18n) : value.text}
              </Option>
            )
          })}
      </MUISelect>
    </Box>
  )
}
