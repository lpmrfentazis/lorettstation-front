import {
  Box,
  List,
  ListItem,
  ListItemButton,
  ListItemContent,
  Typography,
} from '@mui/joy'
import {
  BroadcastIcon,
  ChecklistIcon,
  FileDirectoryIcon,
  GearIcon,
  HomeIcon,
  ServerIcon,
  SlidersIcon,
} from '@primer/octicons-react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'

import { useControlStore } from '@/store/controlStore'

const routes = [
  {
    path: '/',
    i18n: 'dynamic.route.main',
    icon: <HomeIcon size={16} />,
  },
  {
    path: '/schedule',
    i18n: 'dynamic.route.schedule',
    icon: <ChecklistIcon size={16} />,
  },
  {
    path: '/telemetry',
    i18n: 'dynamic.route.telemetry',
    icon: <BroadcastIcon size={16} />,
  },
  {
    path: '/control',
    i18n: 'dynamic.route.control',
    icon: <SlidersIcon size={16} />,
  },
  {
    path: '/logs',
    i18n: 'dynamic.route.logs',
    icon: <ServerIcon size={16} />,
  },
  {
    path: '/explorer',
    i18n: 'dynamic.route.explorer',
    icon: <FileDirectoryIcon size={16} />,
  },
  {
    path: '/settings',
    i18n: 'dynamic.route.settings',
    icon: <GearIcon size={16} />,
  },
]

export const RouteList = () => {
  const { t } = useTranslation()
  const hasControlPanel = useControlStore(state => state.hasControlPanel)

  return (
    <Box
      sx={{
        minHeight: 0,
        overflow: 'hidden auto',

        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <List
        size="sm"
        sx={{
          gap: { xs: '3px', lg: 1 },
          '--ListItem-radius': '5px',
        }}
      >
        {routes.map((route, idx) => {
          if (route.path === '/control' && !hasControlPanel) return null
          return (
            <Link
              key={idx}
              to={route.path}
            >
              <ListItem>
                <ListItemButton>
                  {route.icon}
                  <ListItemContent>
                    <Typography level="title-sm">{t(route.i18n)}</Typography>
                  </ListItemContent>
                </ListItemButton>
              </ListItem>
            </Link>
          )
        })}
      </List>
    </Box>
  )
}
