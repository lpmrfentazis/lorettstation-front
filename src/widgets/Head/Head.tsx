import { Helmet } from 'react-helmet'
import { useTranslation } from 'react-i18next'
import { useLocation } from 'react-router-dom'

import { headRoutes } from '@/config/routes'

import LorettIcon from '@/assets/icons/favicon.ico'

export const Head = () => {
  const { pathname } = useLocation()
  const { t } = useTranslation()

  return (
    <Helmet
      defaultTitle="Lorett"
      titleTemplate="Lorett | %s"
    >
      {headRoutes[pathname] && <title>{t(headRoutes[pathname].title)}</title>}
      {headRoutes[pathname] ? (
        <link
          rel="icon"
          href={headRoutes[pathname].icon}
          type="image/svg+xml"
        ></link>
      ) : (
        <link
          rel="icon"
          href={LorettIcon}
          type="image/svg+xml"
        ></link>
      )}
    </Helmet>
  )
}
