import { useSearchParams } from 'react-router-dom'

import { MODALS } from '@/config/routes'

import { FlyInfoModal } from './FlyInfoModal/FlyInfoModal'
import { FlySettingsModal } from './FlySettingsModal'
import { LimitSettingsModal } from './LimitSettingsModal'
import { LoginModal } from './LoginModal'
import { UnavaibleControlModal } from './UnavaibleControlModal'
import { UnavaibleExplorerModal } from './UnavaibleExplorerModal'

export const Modals = () => {
  const [searchParams] = useSearchParams()
  const modal = searchParams.get('modal')

  return (
    <>
      <UnavaibleControlModal isOpen={modal === MODALS.UnavailableControl} />
      <UnavaibleExplorerModal isOpen={modal === MODALS.UnavailableExplorer} />
      <FlySettingsModal isOpen={modal === MODALS.FlySettings} />
      <FlyInfoModal isOpen={modal === MODALS.FlyInfo} />
      <LoginModal isOpen={modal === MODALS.Login} />
      <LimitSettingsModal isOpen={modal === MODALS.LimitSettings} />
    </>
  )
}
