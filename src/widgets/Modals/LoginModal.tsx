import { zodResolver } from '@hookform/resolvers/zod'
import { Button, Stack } from '@mui/joy'
import { useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'

import { useLogin } from '@/api/login'

import { LoginSchema, TLoginSchema } from '@/types/login'
import { ModalProps } from '@/types/main'

import { Input } from '../Input/Input'
import { BaseModal } from './BaseModal'

export const LoginModal = ({ isOpen }: ModalProps) => {
  const { mutate: login, isPending } = useLogin()
  const navigate = useNavigate()
  const { register, handleSubmit } = useForm<TLoginSchema>({
    resolver: zodResolver(LoginSchema),
  })

  const onLoginClick = ({ username, password }: TLoginSchema) => {
    login({ username, password })
  }

  return (
    <BaseModal
      isOpen={isOpen}
      onClose={() => navigate(-1)}
      modalWidth={300}
    >
      <form onSubmit={handleSubmit(onLoginClick)}>
        <Stack rowGap={'15px'}>
          <Input
            tabIndex={0}
            label="Username"
            {...register('username')}
          />
          <Input
            tabIndex={1}
            label="Password"
            type="password"
            {...register('password')}
          />
          <Button
            tabIndex={2}
            type="submit"
            loading={isPending}
          >
            Войти
          </Button>
        </Stack>
      </form>
    </BaseModal>
  )
}
