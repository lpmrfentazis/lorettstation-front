import { Button, Stack, Typography } from '@mui/joy'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'

import { BaseModal } from './BaseModal'

export const UnavaibleControlModal = ({
  isOpen = false,
}: {
  isOpen: boolean
}) => {
  const { t } = useTranslation()
  const navigate = useNavigate()

  const onReturn = () => {
    navigate(-1)
  }

  return (
    <BaseModal
      isOpen={isOpen}
      onClose={onReturn}
      title={t('modals.unavailableControl.title')}
      modalWidth={320}
    >
      <Stack gap={'10px'}>
        <Typography>{t('modals.unavailableControl.text')}</Typography>
        <Button
          variant="outlined"
          color="primary"
          onClick={onReturn}
        >
          {t('modals.unavailableControl.return')}
        </Button>
      </Stack>
    </BaseModal>
  )
}
