import { zodResolver } from '@hookform/resolvers/zod'
import { Button, Stack } from '@mui/joy'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import { toast } from 'sonner'
import { z } from 'zod'

import { useScheduleStore } from '@/store/scheduleStore'

import { Input } from '../Input/Input'
import { BaseModal } from './BaseModal'

const LimitFormSchema = z.object({
  logsLimit: z.number(),
  scheduleLimit: z.number(),
})

type TLimitFormSchema = z.infer<typeof LimitFormSchema>

export const LimitSettingsModal = ({ isOpen }: { isOpen: boolean }) => {
  const navigate = useNavigate()
  const { t } = useTranslation()
  const { logsLimit, scheduleLimit, setLimits } = useScheduleStore(state => ({
    logsLimit: state.logsLimit,
    scheduleLimit: state.scheduleLimit,
    setLimits: state.setLimits,
  }))
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<TLimitFormSchema>({
    resolver: zodResolver(LimitFormSchema),
    values: {
      logsLimit,
      scheduleLimit,
    },
  })

  const onSave = (data: TLimitFormSchema) => {
    setLimits(data)
    toast.info(t('toast.scheduleUpdate'))
    navigate(-1)
  }

  return (
    <BaseModal
      isOpen={isOpen}
      onClose={() => navigate(-1)}
      modalWidth={300}
    >
      <form onSubmit={handleSubmit(onSave)}>
        <Stack rowGap={'15px'}>
          <Input
            {...register('scheduleLimit', { valueAsNumber: true })}
            label={t('modals.limit.scheduleLimit')}
            errorLabel={errors.scheduleLimit?.message}
          />
          <Input
            {...register('logsLimit', { valueAsNumber: true })}
            label={t('modals.limit.logsLimit')}
            errorLabel={errors.logsLimit?.message}
          />
          <Button type="submit">Сохранить</Button>
        </Stack>
      </form>
    </BaseModal>
  )
}
