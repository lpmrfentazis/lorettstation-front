import { zodResolver } from '@hookform/resolvers/zod'
import { Box, Button, Grid, Stack, Typography } from '@mui/joy'
import { useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useLocation, useNavigate } from 'react-router-dom'
import { z } from 'zod'

import { usePlannedScheduleFull, useUpdateSchedule } from '@/api/schedule'

import { TIME_ZONE_OFFSET } from '@/config/constants'

import { useCheckRights } from '@/hooks/checkRights'

import { ModalProps } from '@/types/main'
import { PlannedFullReqSchema, TPlannedFlySchema } from '@/types/schedule'

import { Input } from '../Input/Input'
import { Loader } from '../Loader/Loader'
import PolarSettingsWidget from '../PolarWidget/PolarSettingsWidget'
import { findClosestPointsByTime } from '../PolarWidget/pixi/lib/utils'
import { BaseModal } from './BaseModal'

type FormSchema = {
  timeStart: string
  timeEnd: string
}

function roundTime(date: string, offset: number = 0) {
  if (!date) return
  const curDate = new Date(date).getTime()
  const localISOTime = new Date(curDate - TIME_ZONE_OFFSET + offset)
    .toISOString()
    .split('.')[0]

  return localISOTime
}

export const FlySettingsModal = ({ isOpen }: ModalProps) => {
  const { t } = useTranslation()
  const hasAccessRights = useCheckRights(2)

  const navigate = useNavigate()
  const { state } = useLocation() as { state: TPlannedFlySchema }

  const { data, isLoading } = usePlannedScheduleFull(
    PlannedFullReqSchema.nullish().parse(state),
  )
  const { updateSchedule } = useUpdateSchedule()

  const {
    register,
    handleSubmit,
    trigger,
    watch,
    formState: { errors },
  } = useForm<FormSchema>({
    resolver: zodResolver(
      z
        .object({
          timeStart: z.string().refine(
            date => {
              if (
                new Date(date) >= new Date(state?.trueTimeStart) &&
                new Date(date) <= new Date(state?.trueTimeEnd)
              )
                return true
            },
            { params: { type: 'incorrect_date_range' } },
          ),
          timeEnd: z.string().refine(
            date => {
              if (
                new Date(date) <= new Date(state?.trueTimeEnd) &&
                new Date(date) >= new Date(state?.trueTimeStart)
              )
                return true
            },
            { params: { type: 'incorrect_date_range' } },
          ),
        })
        .refine(
          data => {
            if (new Date(data.timeStart) <= new Date(data.timeEnd)) return true
          },
          { path: ['timeEnd'], params: { type: 'incorrect_date_range' } },
        ),
    ),
    values: {
      timeStart: roundTime(state?.timeStart, 1000) || '',
      timeEnd: roundTime(state?.timeEnd) || '',
    },
  })

  const onSave = (data: FormSchema) => {
    if (!state) return
    updateSchedule({ ...state, ...data })
  }

  const result = findClosestPointsByTime({
    track: data?.track || [],
    timeStartDate: new Date(watch('timeStart')).getTime(),
    timeEndDate: new Date(watch('timeEnd')).getTime(),
  })

  useEffect(() => {
    trigger('timeStart')
    trigger('timeEnd')
  }, [watch('timeStart'), watch('timeEnd')])

  return (
    <BaseModal
      isOpen={isOpen}
      onClose={() => navigate(-1)}
      title={`${t('modals.flySettings.title')} ${state?.satName}`}
      modalWidth={900}
      modalHeight={800}
    >
      <Box sx={{ overflowX: 'hidden', m: '0 auto' }}>
        <Stack justifyContent={'center'}>
          {isLoading && <Loader />}
          {data && (
            <PolarSettingsWidget
              timeStart={watch('timeStart')}
              timeEnd={watch('timeEnd')}
              track={data.track}
            />
          )}
          <form onSubmit={handleSubmit(onSave)}>
            <Grid
              container
              sx={{ py: '10px' }}
              columnSpacing={'15px'}
            >
              <Grid xs={12}>
                <Typography
                  level="body-sm"
                  fontWeight={'bold'}
                >
                  {t('modals.flySettings.time')}
                </Typography>
              </Grid>
              <Grid
                xs={12}
                sm={6}
              >
                <Input
                  label={t('modals.flySettings.start')}
                  labelLavel="body-sm"
                  slotProps={{
                    input: {
                      step: 1,
                      min: state?.trueTimeStart,
                      max: state?.trueTimeEnd,
                    },
                  }}
                  type="datetime-local"
                  {...register('timeStart')}
                  errorLabel={errors.timeStart?.message}
                />
              </Grid>
              <Grid
                xs={12}
                sm={6}
              >
                <Input
                  label={t('modals.flySettings.stop')}
                  labelLavel="body-sm"
                  slotProps={{
                    input: {
                      step: 1,
                    },
                  }}
                  type="datetime-local"
                  {...register('timeEnd')}
                  errorLabel={errors.timeEnd?.message}
                />
              </Grid>
            </Grid>
            <Grid
              container
              sx={{ py: '10px' }}
              columnSpacing={'15px'}
              alignItems={'end'}
            >
              <Grid xs={12}>
                <Typography
                  level="body-sm"
                  fontWeight={'bold'}
                >
                  {t('modals.flySettings.elevation')}
                </Typography>
              </Grid>
              <Grid
                xs={12}
                sm={6}
              >
                <Typography
                  fontWeight={'bold'}
                  level="body-sm"
                  sx={{ mb: '5px' }}
                >
                  {t('modals.flySettings.start')}
                </Typography>
                <Box
                  sx={{
                    border: '1px solid',
                    borderRadius: '6px',
                    px: '12px',
                    lineHeight: '24px',
                    height: '34px',
                    display: 'flex',
                    alignItems: 'center',
                  }}
                >
                  {result?.timeStartPoint.elevation.toFixed(3)}
                </Box>
              </Grid>
              <Grid
                xs={12}
                sm={6}
              >
                <Typography
                  fontWeight={'bold'}
                  level="body-sm"
                  sx={{ mb: '5px' }}
                >
                  {t('modals.flySettings.stop')}
                </Typography>
                <Box
                  sx={{
                    border: '1px solid',
                    borderRadius: '6px',
                    px: '12px',
                    lineHeight: '24px',
                    height: '34px',
                    display: 'flex',
                    alignItems: 'center',
                  }}
                >
                  {result?.timeEndPoint.elevation.toFixed(3)}
                </Box>
              </Grid>
            </Grid>
            <Stack alignItems={'center'}>
              <Box>
                <Button
                  disabled={!hasAccessRights}
                  sx={{ mt: '10px' }}
                  type="submit"
                >
                  {t('main.save')}
                </Button>
              </Box>
            </Stack>
          </form>
        </Stack>
      </Box>
    </BaseModal>
  )
}
