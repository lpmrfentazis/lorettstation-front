import {
  DialogContent,
  DialogTitle,
  Modal,
  ModalClose,
  ModalDialog,
} from '@mui/joy'
import { AnimatePresence, motion } from 'framer-motion'

import { useDeviceSize } from '@/hooks/size'

export const BaseModal = ({
  children,
  isOpen,
  onClose,
  title,
  modalWidth,
  modalHeight,
}: {
  children: React.ReactNode
  isOpen: boolean
  onClose: () => void
  title?: string
  modalWidth?: number
  modalHeight?: number
}) => {
  const { width, height } = useDeviceSize()

  return (
    <AnimatePresence>
      {isOpen && (
        <Modal
          onClose={onClose}
          open={isOpen}
          slotProps={{
            backdrop: {
              sx: {
                backdropFilter: 'none',
              },
            },
          }}
        >
          <ModalDialog
            component={motion.div}
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
            transition={{ duration: 0.25 }}
            layout={
              width > (modalWidth || 0) && height > (modalHeight || 0)
                ? 'center'
                : 'fullscreen'
            }
            sx={{
              // overflow: 'hidden',
              minWidth: Math.min(modalWidth || 0, width),
              // minHeight: modalHeight || 0,
            }}
          >
            <ModalClose
              sx={{ zIndex: 10 }}
              slotProps={{ root: { tabIndex: -1 } }}
            />
            {title && <DialogTitle>{title}</DialogTitle>}
            <DialogContent sx={{ width: '100%', height: '100%' }}>
              {children}
            </DialogContent>
          </ModalDialog>
        </Modal>
      )}
    </AnimatePresence>
  )
}
