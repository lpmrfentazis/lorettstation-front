import { Explorer } from '@/widgets/Explorer/Explorer'
import { Loader } from '@/widgets/Loader/Loader'

import { useExplorerPath } from '@/api/explorer'

export const ExplorerTab = ({ passId }: { passId: string }) => {
  const { data, isLoading } = useExplorerPath(passId)

  return (
    <>
      {isLoading && <Loader />}
      {!isLoading && (
        <Explorer
          inlineModal
          path={data}
        />
      )}
    </>
  )
}
