import PolarInfoWidget from '@/widgets/PolarWidget/PolarInfoWidget'
import {
  Box,
  Tab,
  TabList,
  TabPanel,
  Tabs,
  Typography,
  tabClasses,
} from '@mui/joy'
import { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation, useNavigate } from 'react-router-dom'

import { usePassedScheduleFull } from '@/api/schedule'

import { ModalProps } from '@/types/main'

import { CodeHighlighter } from '../../CodeHighlighter/CodeHighlighter'
import { Loader } from '../../Loader/Loader'
import { BaseModal } from '../BaseModal'
import { ExplorerTab } from './ExplorerTab'

export const FlyInfoModal = ({ isOpen }: ModalProps) => {
  const { state } = useLocation() as { state: { passID: string } }
  const { t } = useTranslation()
  const navigate = useNavigate()
  const { data, isLoading } = usePassedScheduleFull(state?.passID)
  // const data = TEST_OBJ

  const Code = useMemo(() => {
    if (!data) return null
    return <CodeHighlighter code={JSON.stringify(data, null, 2)} />
  }, [data])

  return (
    <BaseModal
      isOpen={isOpen}
      onClose={() => navigate(-1)}
      modalWidth={900}
      modalHeight={800}
    >
      <Box sx={{ overflow: 'hidden', height: '100%' }}>
        <Tabs
          defaultValue={0}
          sx={{
            bgcolor: 'transparent',
            height: '100%',
          }}
        >
          <TabList
            tabFlex={1}
            size="sm"
            sx={{
              px: 0,
              justifyContent: 'left',
              [`&& .${tabClasses.root}`]: {
                fontWeight: '600',
                flex: 'initial',
                color: 'text.tertiary',
                [`&.${tabClasses.selected}`]: {
                  bgcolor: 'transparent',
                  color: 'text.primary',
                  '&::after': {
                    height: '2px',
                    bgcolor: 'primary.500',
                  },
                },
              },
            }}
          >
            <Tab
              sx={{ borderRadius: '6px 6px 0 0', px: 2 }}
              indicatorInset
              value={0}
            >
              {t('modals.flyInfo.log')}
            </Tab>
            <Tab
              sx={{ borderRadius: '6px 6px 0 0', px: 2 }}
              indicatorInset
              value={1}
            >
              {t('modals.flyInfo.scheme')}
            </Tab>
            <Tab
              sx={{ borderRadius: '6px 6px 0 0', px: 2 }}
              indicatorInset
              value={2}
            >
              {t('modals.flyInfo.data')}
            </Tab>
          </TabList>
          <TabPanel
            sx={{ px: 0, overflow: 'auto', height: '100%' }}
            value={0}
          >
            {isLoading && <Loader />}
            {data && <Box>{Code}</Box>}
          </TabPanel>
          <TabPanel
            sx={{ px: 0, overflow: 'auto', height: '100%' }}
            value={1}
          >
            <Typography sx={{ mb: '15px' }}>{data?.passID}</Typography>
            <Box sx={{ maxWidth: '80%', m: '0 auto' }}>
              {data && <PolarInfoWidget points={data.recordInfo.log} />}
            </Box>
          </TabPanel>

          <TabPanel
            sx={{ px: 0, overflow: 'auto', height: '100%' }}
            value={2}
          >
            <ExplorerTab passId={state?.passID} />
          </TabPanel>
        </Tabs>
      </Box>
    </BaseModal>
  )
}
