import { Button, Stack, Typography } from '@mui/joy'
import { useQueryClient } from '@tanstack/react-query'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'

import { BaseModal } from './BaseModal'

export const UnavaibleExplorerModal = ({
  isOpen = false,
}: {
  isOpen: boolean
}) => {
  const { t } = useTranslation()
  const navigate = useNavigate()
  const queryClient = useQueryClient()
  // const { isLoading } = useExplorer()

  const onReturn = () => {
    navigate(-1)
  }

  const onUpdate = () => {
    queryClient.invalidateQueries({ queryKey: ['explorer'] })
  }

  return (
    <BaseModal
      isOpen={isOpen}
      onClose={onReturn}
      title={t('modals.unavailableExplorer.title')}
      modalWidth={320}
    >
      <Stack gap={'10px'}>
        <Typography>{t('modals.unavailableExplorer.text')}</Typography>
        <Button
          variant="outlined"
          color="danger"
          onClick={onUpdate}
          // loading={isLoading}
        >
          {t('modals.unavailableExplorer.update')}
        </Button>
      </Stack>
    </BaseModal>
  )
}
