import { Grid, IconButton, Stack } from '@mui/joy'
import { ArrowLeftIcon, ArrowRightIcon } from '@primer/octicons-react'
import { useCallback, useState } from 'react'
import { useTranslation } from 'react-i18next'

import { SatellitesWidgetColumn } from './SatellitesWidgetColumn'

export const SatellitesWidget = ({
  leftColumnn,
  rightColumn,
  setActiveList,
}: {
  leftColumnn: string[]
  rightColumn: string[]
  setActiveList: (newSatlist: string[]) => void
}) => {
  const { t } = useTranslation()
  const [leftSelection, setLeftSelection] = useState<string[]>([])
  const [rightSelection, setRightSelection] = useState<string[]>([])
  const [filter, setFilter] = useState<{
    leftFilter: string
    rightFilter: string
  }>({ leftFilter: '', rightFilter: '' })

  const onUpdateLeft = useCallback(() => {
    const newActiveList = rightColumn.filter(
      item => !rightSelection.includes(item),
    )

    setActiveList(newActiveList)
    setRightSelection([])
  }, [rightColumn, setActiveList, rightSelection])

  const onUpdateRight = useCallback(() => {
    const newActiveList = rightColumn.concat(...leftSelection)

    setActiveList(newActiveList)
    setLeftSelection([])
  }, [rightColumn, leftSelection, setActiveList])

  return (
    <Grid
      container
      alignItems={'stretch'}
    >
      <Grid xs={5}>
        <SatellitesWidgetColumn
          title={t('settings.all')}
          values={leftColumnn.filter(item => !rightColumn.includes(item))}
          selected={leftSelection}
          setSelected={setLeftSelection}
          filter={filter.leftFilter}
          setFilter={value =>
            setFilter(prev => ({ ...prev, leftFilter: value }))
          }
        />
      </Grid>
      <Grid xs={2}>
        <Stack alignItems={'center'}>
          <Stack direction={'row'}>
            <IconButton onClick={onUpdateLeft}>
              <ArrowLeftIcon size={24} />
            </IconButton>
          </Stack>
          <Stack direction={'row'}>
            <IconButton onClick={onUpdateRight}>
              <ArrowRightIcon size={24} />
            </IconButton>
          </Stack>
        </Stack>
      </Grid>

      <Grid xs={5}>
        <SatellitesWidgetColumn
          title={t('settings.active')}
          values={rightColumn}
          selected={rightSelection}
          setSelected={setRightSelection}
          filter={filter.rightFilter}
          setFilter={value =>
            setFilter(prev => ({ ...prev, rightFilter: value }))
          }
        />
      </Grid>
    </Grid>
  )
}
