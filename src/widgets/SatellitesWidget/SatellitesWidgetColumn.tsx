import { Box, Card, Stack } from '@mui/joy'
import { SearchIcon } from '@primer/octicons-react'
import { useCallback } from 'react'

import { Input } from '../Input/Input'

export const SatellitesWidgetColumn = ({
  title,
  values,
  selected,
  setSelected,
  filter,
  setFilter,
}: {
  title: string
  values: string[]
  selected: string[]
  setSelected: (data: string[]) => void
  filter: string
  setFilter: (value: string) => void
}) => {
  const onToggle = useCallback(
    (value: string) => {
      const idx = selected.indexOf(value)
      if (idx !== -1) {
        const newSelection = [...selected]
        newSelection.splice(idx, 1)
        setSelected([...newSelection])
      } else {
        setSelected([...selected, value])
      }
    },
    [selected, setSelected],
  )

  return (
    <Card sx={{ minHeight: '300px', maxHeight: '400px', overflow: 'auto' }}>
      <Input
        value={filter}
        onChange={e => setFilter(e.target.value)}
        startDecorator={<SearchIcon size={16} />}
        className="satlist-text"
        label={title}
        labelLavel="body-sm"
      />
      <Stack rowGap={'5px'}>
        {values
          .filter(item => item.includes(filter))
          .map((item, idx) => {
            return (
              <SatellitesWidgetItem
                key={idx}
                title={item}
                isSelected={selected.includes(item)}
                setSelected={() => onToggle(item)}
              />
            )
          })}
      </Stack>
    </Card>
  )
}

export const SatellitesWidgetItem = ({
  title,
  isSelected,
  setSelected,
}: {
  title: string
  isSelected?: boolean
  setSelected: () => void
}) => {
  return (
    <Box
      onClick={setSelected}
      className="satlist-text"
      sx={{
        background: isSelected
          ? 'var(--joy-palette-primary-softActiveBg)'
          : 'transparent',
        p: '5px 10px',
        borderRadius: '10px',
        cursor: 'pointer',
        ':hover': {
          background: 'var(--joy-palette-primary-outlinedActiveBg)',
        },
      }}
    >
      {title}
    </Box>
  )
}
