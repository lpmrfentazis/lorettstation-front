import { Skeleton, Stack, Tooltip, Typography } from '@mui/joy'

export const StatusBarItem = ({
  title,
  tooltipTitle,
  value,
  loading,
  tooltipPlacement = 'top-start',
}: {
  title: string
  tooltipTitle?: string
  value?: string
  loading: boolean
  tooltipPlacement?: 'bottom-start' | 'top-start'
}) => {
  return (
    <Stack
      direction="row"
      justifyContent="space-between"
    >
      <Typography fontWeight={'bold'}>{title}:</Typography>
      {tooltipTitle ? (
        <Tooltip
          title={tooltipTitle}
          variant="plain"
          placement={tooltipPlacement}
          size="lg"
          sx={{
            fontWeight: 'bold',
            color: 'var(--joy-palette-neutral-500, #636B74)',
          }}
        >
          <Typography
            fontWeight={'bold'}
            sx={{ color: 'var(--joy-palette-neutral-500, #636B74)' }}
            component={'div'}
            width={100}
            textAlign={'right'}
          >
            <Skeleton
              animation="wave"
              variant="text"
              loading={loading}
            >
              {value}
            </Skeleton>
          </Typography>
        </Tooltip>
      ) : (
        <Typography
          fontWeight={'bold'}
          sx={{ color: 'var(--joy-palette-neutral-500, #636B74)' }}
          component={'div'}
          width={100}
          textAlign={'right'}
        >
          <Skeleton
            animation="wave"
            variant="text"
            loading={loading}
          >
            {value}
          </Skeleton>
        </Typography>
      )}
    </Stack>
  )
}
