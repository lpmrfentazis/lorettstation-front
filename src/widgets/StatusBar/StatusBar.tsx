import { Card, Divider, Skeleton, Stack, Typography } from '@mui/joy'
import { useEffect, useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'

import { TIME_ZONE, getDate, getTime } from '@/utils/date'

import { TStatusResSchema } from '@/types/status'

import { StatusBarItem } from './StatusBarItem'

export const StatusBar = ({ status }: { status?: TStatusResSchema }) => {
  const [currentDate, setCurrentDate] = useState<number>(status?.date || 0)
  const intervalRef = useRef<null | number>(null)
  const { t } = useTranslation()

  useEffect(() => {
    if (!status?.date) return
    setCurrentDate(status.date)
  }, [status?.date])

  useEffect(() => {
    intervalRef.current = setInterval(() => {
      setCurrentDate(prev => prev + 1000)
    }, 1000)

    return () => {
      if (!intervalRef.current) return
      clearInterval(intervalRef.current)
    }
  }, [])

  return (
    <>
      <Typography
        level="h2"
        color="neutral"
      >
        <Skeleton
          animation="wave"
          variant="text"
          loading={!status}
          width={100}
        >
          {status?.stationName}
        </Skeleton>
      </Typography>
      <Card
        sx={theme => ({
          background: theme.vars.palette.background.level1,
        })}
      >
        <Stack
          rowGap={'9px'}
          divider={<Divider />}
        >
          <StatusBarItem
            loading={!status}
            title={t('sidebar.time')}
            tooltipTitle={`Local: ${
              status && getTime(currentDate, TIME_ZONE.Our)
            }`}
            value={status && getTime(currentDate)}
          />

          <StatusBarItem
            loading={!status}
            title={t('sidebar.date')}
            tooltipTitle={`Local: ${
              status && getDate(currentDate, TIME_ZONE.Our)
            }`}
            tooltipPlacement="bottom-start"
            value={status && getDate(currentDate)}
          />

          <StatusBarItem
            loading={!status}
            title={t('sidebar.status')}
            value={status?.status}
          />

          {status?.battery && (
            <StatusBarItem
              loading={!status}
              title={t('sidebar.battery')}
              value={`${status.battery}%`}
            />
          )}
        </Stack>
      </Card>
    </>
  )
}
