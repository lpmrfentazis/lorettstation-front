import { useEffect, useState } from 'react'

import { useToggleColorScheme } from '@/hooks/colorScheme'

import { TRecordInfoLogSchema } from '@/types/schedule'

import { CanvasContainer } from '../CanvasContainer/CanvasContainer'
import { BasePolarContainer } from './pixi/BasePolarContainer'
import { PolarInfoContainer } from './pixi/PolarInfoContainer'
import { WIDGET_HEIGHT, WIDGET_WIDTH } from './pixi/lib/constants'

const PolarInfoWidget = ({ points }: { points: TRecordInfoLogSchema[] }) => {
  const [polarWidgetContainer, setPolarWidgetContainer] =
    useState<BasePolarContainer | null>(null)

  const { mode } = useToggleColorScheme()

  useEffect(() => {
    setPolarWidgetContainer(
      new PolarInfoContainer({
        points,
        mode,
      }),
    )

    return () => {
      polarWidgetContainer?.destroy()
    }
  }, [mode])

  return (
    <CanvasContainer
      widget={polarWidgetContainer}
      height={WIDGET_HEIGHT}
      width={WIDGET_WIDTH}
    />
  )
}

export default PolarInfoWidget
