import * as PIXI from 'pixijs'

import { COLORS, TEXT_OPTIONS } from '@/config/constants'

import { toRadians } from '@/utils/calculateAzimuth'

import { ThemeMode } from '@/types/main'

import { CENTER, RADIUS } from './lib/constants'
import { DashLine } from './lib/dashedLine'

export class BasePolarContainer extends PIXI.Container {
  private _mode: ThemeMode = 'light'

  labelContainer = new PIXI.Container()
  private linesContainer: DashLine

  constructor({ mode }: { mode: ThemeMode }) {
    super()

    const g = new PIXI.Graphics()
    this.linesContainer = new DashLine(g, {
      dash: [4, 4],
      width: 1,
      color: 0xf0f4f8,
    })
    this.addChild(g)

    this.addChild(this.labelContainer)

    this.setup()
    this.mode = mode
  }

  get mode() {
    return this._mode
  }

  set mode(mode: ThemeMode) {
    this._mode = mode

    this.labelContainer.children.forEach(item => {
      if (item instanceof PIXI.Text) {
        item.style.fill = COLORS.text[mode]
      }
    })

    this.linesContainer.graphics.tint = COLORS.text[mode]
  }

  private setup() {
    for (let i = 20; i < 90; i += 10) {
      const radius = RADIUS * (1 - i / 90)
      this.linesContainer.drawCircle(CENTER.X, CENTER.Y, radius)

      const label = new PIXI.Text(`${i}°`, TEXT_OPTIONS)
      label.anchor.set(0.5, 0.5)
      label.scale.set(0.5)
      label.position.set(
        CENTER.X + radius * Math.sin(toRadians(15)),
        (CENTER.Y - radius) * Math.cos(toRadians(15)) + 8,
      )
      label.rotation = toRadians(15)
      this.labelContainer.addChild(label)
    }

    for (let angle = 0; angle < 360; angle += 30) {
      const x = CENTER.X + Math.cos(toRadians(angle - 90)) * (RADIUS + 20)
      const y = CENTER.Y + Math.sin(toRadians(angle - 90)) * (RADIUS + 20)
      this.linesContainer.moveTo(CENTER.X, CENTER.Y)
      this.linesContainer.lineTo(x, y)

      const label = new PIXI.Text(`${angle}°`, TEXT_OPTIONS)
      label.anchor.set(0.5, 0.5)
      label.scale.set(0.5)
      label.position.set(
        CENTER.X + Math.cos(toRadians(angle - 90)) * (RADIUS + 30),
        CENTER.X + Math.sin(toRadians(angle - 90)) * (RADIUS + 30),
      )
      label.rotation = toRadians(angle)
      this.labelContainer.addChild(label)
    }
  }
}
