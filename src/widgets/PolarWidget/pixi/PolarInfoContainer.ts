import * as PIXI from 'pixijs'

import { BOLD_TEXT_OPTIONS, COLORS } from '@/config/constants'

import { toRadians } from '@/utils/calculateAzimuth'
import { getTime } from '@/utils/date'

import { ThemeMode } from '@/types/main'
import { TRecordInfoLogSchema } from '@/types/schedule'

import { BasePolarContainer } from './BasePolarContainer'
import { CENTER, POINT_RADIUS, RADIUS, TIME_INTERVAL } from './lib/constants'
import {
  calculateDistance,
  findClosestPoints,
  getPerpendicularPoint,
  SnrPointToHSV,
  HSVToRGBHex
} from './lib/utils'

export class PolarInfoContainer extends BasePolarContainer {
  private pointsContainer = new PIXI.Container()
  points: TRecordInfoLogSchema[]

  constructor({
    mode,
    points,
  }: {
    mode: ThemeMode
    points: TRecordInfoLogSchema[]
  }) {
    super({ mode })

    this.points = points

    this.addChild(this.pointsContainer)
    this.pointsContainer.sortableChildren = true

    this.setupPoints()
  }

  get mode() {
    return super.mode
  }

  set mode(mode: ThemeMode) {
    super.mode = mode
    if (!this.pointsContainer) return

    this.pointsContainer.removeChildren()
    this.setupPoints()
  }

  setupPoints() {
    const Points = new PIXI.Graphics()
    const trajectory = new PIXI.Graphics()

    Points.zIndex = 2
    trajectory.zIndex = 1

    trajectory.lineStyle(3, COLORS.text[this.mode], 1)

    this.pointsContainer.addChild(Points, trajectory)

    const lastPoint = { x: 0, y: 0 }
    let isSetuped = false

    for (let i = 0; i < this.points.length; i++) {
      const point = this.points[i]
      const L = RADIUS * (1- point.elevation/90)

      const x = L * Math.sin(toRadians(point.azimuth)) + CENTER.X
      const y = -L * Math.cos(toRadians(point.azimuth)) + CENTER.Y

      const diff = calculateDistance(lastPoint, { x, y })

      if (diff >= 1.1 * POINT_RADIUS || !isSetuped) {
        const [P1, P2] = findClosestPoints(point.snr)

        const t = point.snr / P2.snr

        const hsv1 = SnrPointToHSV(P1)
        const hsv2 = SnrPointToHSV(P2)

        let hsvOut = hsv1

        hsvOut.H = hsv1.H + t * (hsv2.H - hsv1.H)
        hsvOut.S = hsv1.S + t * (hsv2.S - hsv1.S)
        hsvOut.V = hsv1.V + t * (hsv2.V - hsv1.V)

        Points.beginFill(HSVToRGBHex(hsvOut))
        Points.drawCircle(x, y, POINT_RADIUS)
        Points.endFill()

        lastPoint.x = x
        lastPoint.y = y
      }

      if (!isSetuped) {
        isSetuped = true
        trajectory.moveTo(x, y)
      } else {
        trajectory.lineTo(x, y)
      }
    }

    let lastDate = 0
    const lastDatePoint = { x: 0, y: 0 }
    let lastTimeLabel
    let lastSnrLabel

    for (let i = 0; i < this.points.length; i++) {
      const point = this.points[i]
      const L = RADIUS * (1- point.elevation/90)

      const x = L * Math.sin(toRadians(point.azimuth)) + CENTER.X
      const y = -L * Math.cos(toRadians(point.azimuth)) + CENTER.Y

      const currentDate = new Date(point.timeMoment).getTime()

      if (lastTimeLabel) {
        const { newPoint, angle } = getPerpendicularPoint(
          lastDatePoint,
          {
            x,
            y,
          },
          lastTimeLabel.width / 2 + 15,
        )
        lastTimeLabel.position.set(newPoint.x, newPoint.y)
        lastTimeLabel.rotation = angle
      }

      if (lastSnrLabel) {
        const { newPoint, angle } = getPerpendicularPoint(
          lastDatePoint,
          {
            x,
            y,
          },
          -lastSnrLabel.width / 2 - 15,
        )
        lastSnrLabel.position.set(newPoint.x, newPoint.y)
        lastSnrLabel.rotation = angle
      }

      if (
        currentDate - lastDate > TIME_INTERVAL &&
        i !== this.points.length - 1 &&
        calculateDistance(lastDatePoint, { x, y }) > 4 * POINT_RADIUS
      ) {
        const textTime = new PIXI.Text(getTime(currentDate), BOLD_TEXT_OPTIONS)
        textTime.anchor.set(0.5)
        textTime.scale.set(0.5)
        textTime.style.fill = COLORS.text[this.mode]

        const textSnr = new PIXI.Text(point.snr.toFixed(), BOLD_TEXT_OPTIONS)
        textSnr.anchor.set(0.5)
        textSnr.scale.set(0.5)
        textSnr.style.fill = COLORS.text[this.mode]

        lastDate = currentDate
        lastDatePoint.x = x
        lastDatePoint.y = y
        lastTimeLabel = textTime
        lastSnrLabel = textSnr
        this.labelContainer.addChild(textTime)
        this.labelContainer.addChild(textSnr)
      }
    }
  }
}
