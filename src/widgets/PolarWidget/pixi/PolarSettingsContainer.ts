import * as PIXI from 'pixijs'

import { BOLD_TEXT_OPTIONS, COLORS } from '@/config/constants'

import { toRadians } from '@/utils/calculateAzimuth'
import { getTime } from '@/utils/date'

import { ThemeMode } from '@/types/main'
import { TPlannedFlyPointSchema } from '@/types/schedule'

import { BasePolarContainer } from './BasePolarContainer'
import {
  CENTER,
  COLOR_PRIMARY,
  POINT_RADIUS,
  RADIUS,
  TIME_INTERVAL,
} from './lib/constants'
import {
  calculateDistance,
  findClosestPointsByTime,
  getPerpendicularPoint,
} from './lib/utils'

export class PolarSettingsContainer extends BasePolarContainer {
  private trajectoryContainer = new PIXI.Container()
  track: TPlannedFlyPointSchema[]
  timeStart: string
  timeEnd: string

  constructor({
    mode,
    track,
    timeStart,
    timeEnd,
  }: {
    mode: ThemeMode
    track: TPlannedFlyPointSchema[]
    timeStart: string
    timeEnd: string
  }) {
    super({ mode })

    this.track = track
    this.timeStart = timeStart
    this.timeEnd = timeEnd

    this.addChild(this.trajectoryContainer)

    this.setupPoints()
  }

  get mode() {
    return super.mode
  }

  set mode(mode: ThemeMode) {
    super.mode = mode
    if (!this.trajectoryContainer) return

    this.trajectoryContainer.removeChildren()
    this.setupPoints()
  }

  setupPoints() {
    const trajectory = new PIXI.Graphics()

    trajectory.lineStyle(5, COLORS.text[this.mode], 1)

    this.trajectoryContainer.addChild(trajectory)

    const timeStartDate = new Date(this.timeStart).getTime()
    const timeEndDate = new Date(this.timeEnd).getTime()

    let isSetuped = false

    for (let i = 0; i < this.track.length; i += 5) {
      const point = this.track[i]
      const L = RADIUS * (1 - point.elevation / 90)

      const x = L * Math.sin(toRadians(point.azimuth)) + CENTER.X
      const y = -L * Math.cos(toRadians(point.azimuth)) + CENTER.Y

      if (!isSetuped) {
        trajectory.moveTo(x, y)
        isSetuped = true
      } else {
        const curDate = new Date(point.timeMoment).getTime()
        if (curDate < timeEndDate && curDate > timeStartDate) {
          trajectory.lineStyle(5, COLOR_PRIMARY, 1)
        } else {
          trajectory.lineStyle(5, COLORS.text[this.mode], 1)
        }

        trajectory.lineTo(x, y)
      }
    }

    let lastDate = 0
    const lastDatePoint = { x: 0, y: 0 }
    let lastTimeLabel

    for (let i = 0; i < this.track.length; i++) {
      const point = this.track[i]
      const L = RADIUS * (1 - point.elevation / 90)

      const x = L * Math.sin(toRadians(point.azimuth)) + CENTER.X
      const y = -L * Math.cos(toRadians(point.azimuth)) + CENTER.Y

      const currentDate = new Date(point.timeMoment).getTime()

      if (lastTimeLabel) {
        const { newPoint, angle } = getPerpendicularPoint(
          lastDatePoint,
          {
            x,
            y,
          },
          lastTimeLabel.width / 2 + 15,
        )
        lastTimeLabel.position.set(newPoint.x, newPoint.y)
        lastTimeLabel.rotation = angle
      }

      if (
        currentDate - lastDate > TIME_INTERVAL &&
        i !== this.track.length - 1 &&
        calculateDistance(lastDatePoint, { x, y }) > 4 * POINT_RADIUS
      ) {
        const textTime = new PIXI.Text(getTime(currentDate), BOLD_TEXT_OPTIONS)
        textTime.anchor.set(0.5)
        textTime.scale.set(0.5)
        textTime.style.fill = COLORS.text[this.mode]

        lastDate = currentDate
        lastDatePoint.x = x
        lastDatePoint.y = y
        lastTimeLabel = textTime
        this.labelContainer.addChild(textTime)
      }
    }

    const res = findClosestPointsByTime({
      timeStartDate,
      timeEndDate,
      track: this.track,
    })

    if (!res) return
    const { timeStartPoint, timeEndPoint } = res

    let L = RADIUS * (1 - timeStartPoint.elevation / 90)
    let x = L * Math.sin(toRadians(timeStartPoint.azimuth)) + CENTER.X
    let y = -L * Math.cos(toRadians(timeStartPoint.azimuth)) + CENTER.Y

    trajectory.beginFill(COLOR_PRIMARY)
    trajectory.drawCircle(x, y, POINT_RADIUS)
    trajectory.endFill()

    L = RADIUS * (1 - timeEndPoint.elevation / 90)
    x = L * Math.sin(toRadians(timeEndPoint.azimuth)) + CENTER.X
    y = -L * Math.cos(toRadians(timeEndPoint.azimuth)) + CENTER.Y

    trajectory.beginFill(COLOR_PRIMARY)
    trajectory.drawCircle(x, y, POINT_RADIUS)
    trajectory.endFill()
  }
}
