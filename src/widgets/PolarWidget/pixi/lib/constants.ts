export const WIDGET_WIDTH = 1000
export const WIDGET_HEIGHT = 1000

export type SnrPoint = { snr: number; R: number; G: number; B: number }

export const RADIUS = 425

export const CENTER = {
  X: Math.floor(WIDGET_WIDTH / 2),
  Y: Math.floor(WIDGET_HEIGHT / 2),
} as const

export const POINT_RADIUS = 8
export const TIME_INTERVAL = 5 * 1000

export const COLOR_PRIMARY = 0x0b6bcb
