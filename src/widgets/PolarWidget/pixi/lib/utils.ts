import { POLAR_PALLETE } from '@/config/constants'

import { TPlannedFlyPointSchema } from '@/types/schedule'

import { SnrPoint } from './constants'

export function findClosestPoints(point: number): SnrPoint[] {
  let closestPoints

  for (let i = 0; i < POLAR_PALLETE.length - 2; i++) {
    if (POLAR_PALLETE[i].snr <= point) {
      closestPoints = [POLAR_PALLETE[i], POLAR_PALLETE[i + 1]]
    }
  }

  if (closestPoints) return closestPoints

  const last = POLAR_PALLETE[POLAR_PALLETE.length - 1]
  return [last, last]
}
export function RGBArrayToHex(rgbArray: [number, number, number]) {
  return (rgbArray[0] << 16) + (rgbArray[1] << 8) + rgbArray[2]
}

type Point = {
  x: number
  y: number
}

export function calculateDistance(point1: Point, point2: Point): number {
  const xDiff = point2.x - point1.x
  const yDiff = point2.y - point1.y

  return Math.sqrt(xDiff ** 2 + yDiff ** 2)
}

export function getPerpendicularPoint(
  point1: Point,
  point2: Point,
  offset: number,
): { newPoint: Point; angle: number } {
  const deltaX = point2.x - point1.x
  const deltaY = point2.y - point1.y

  const { x: x1, y: y1 } = point1
  const { x: x2, y: y2 } = point2

  const mp = -(x2 - x1) / (y2 - y1)
  const b1 = y1 - mp * x1
  const n = b1 - y1

  const S = offset
  // D = b^2 - 4ac

  const a = 1 + mp ** 2
  const b = 2 * mp * n - 2 * x1
  const c = x1 ** 2 + n ** 2 - S ** 2
  const D = b ** 2 - 4 * a * c

  const xr1 = (-b - D ** (1 / 2)) / (2 * a)
  const xr2 = (-b + D ** (1 / 2)) / (2 * a)

  const yr1 = mp * xr1 + b1
  const yr2 = mp * xr2 + b1

  // Угол поворота относительно горизонтали
  const angle = Math.atan2(deltaY, deltaX) + Math.PI / 2

  return {
    // newPoint: { x: newPointX, y: newPointY },
    newPoint: offset > 0 ? { x: xr2, y: yr2 } : { x: xr1, y: yr1 },
    angle: point2.y > point1.y ? Math.PI + angle : angle,
  }
}

export const findClosestPointsByTime = ({
  timeStartDate,
  timeEndDate,
  track,
}: {
  timeStartDate: number
  timeEndDate: number
  track: TPlannedFlyPointSchema[]
}) => {
  if (track.length === 0) return null

  let minDiffStart = Infinity
  let minDiffStartIdx = 0
  let minDiffEnd = Infinity
  let minDiffEndIdx = track.length - 1

  for (let i = 0; i < track.length; i++) {
    const point = track[i]
    const curDate = new Date(point.timeMoment).getTime()

    if (Math.abs(curDate - timeStartDate) < minDiffStart) {
      minDiffStart = Math.abs(curDate - timeStartDate)
      minDiffStartIdx = i
    }

    if (Math.abs(curDate - timeEndDate) < minDiffEnd) {
      minDiffEnd = Math.abs(curDate - timeEndDate)
      minDiffEndIdx = i
    }
  }

  const timeStartPoint = track[minDiffStartIdx]
  const timeEndPoint = track[minDiffEndIdx]

  return { timeStartPoint, timeEndPoint }
}

export interface HSV {
  H: number
  S: number
  V: number
}

export function SnrPointToHSV({ R, G, B }: SnrPoint): HSV {
  R /= 255
  G /= 255
  B /= 255
  const max = Math.max(R, G, B)
  const min = Math.min(R, G, B)
  const V = max
  const d = max - min
  const S = max === 0 ? 0 : d / max
  let H = 0

  if (max !== min) {
    switch (max) {
      case R:
        H = (G - B) / d + (G < B ? 6 : 0)
        break
      case G:
        H = (B - R) / d + 2
        break
      case B:
        H = (R - G) / d + 4
        break
    }
    H /= 6
  }

  return { H, S, V }
}

export function HSVToRGBArray({ H, S, V }: HSV): [number, number, number] {
  let R: number = 0,
    G: number = 0,
    B: number = 0

  const i = Math.floor(H * 6)
  const f = H * 6 - i
  const p = V * (1 - S)
  const q = V * (1 - f * S)
  const t = V * (1 - (1 - f) * S)

  switch (i % 6) {
    case 0:
      R = V
      G = t
      B = p
      break
    case 1:
      R = q
      G = V
      B = p
      break
    case 2:
      R = p
      G = V
      B = t
      break
    case 3:
      R = p
      G = q
      B = V
      break
    case 4:
      R = t
      G = p
      B = V
      break
    case 5:
      R = V
      G = p
      B = q
      break
  }

  return [Math.round(R * 255), Math.round(G * 255), Math.round(B * 255)]
}

export function HSVToRGBHex(hsv: HSV) {
  const rgbArray = HSVToRGBArray(hsv)
  return (rgbArray[0] << 16) + (rgbArray[1] << 8) + rgbArray[2]
}
