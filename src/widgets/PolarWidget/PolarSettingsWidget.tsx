import { useEffect, useState } from 'react'

import { useToggleColorScheme } from '@/hooks/colorScheme'

import { TPlannedFlyPointSchema } from '@/types/schedule'

import { CanvasContainer } from '../CanvasContainer/CanvasContainer'
import { BasePolarContainer } from './pixi/BasePolarContainer'
import { PolarSettingsContainer } from './pixi/PolarSettingsContainer'
import { WIDGET_HEIGHT, WIDGET_WIDTH } from './pixi/lib/constants'

const PolarSettingsWidget = ({
  track,
  timeEnd,
  timeStart,
}: {
  timeEnd: string
  timeStart: string
  track: TPlannedFlyPointSchema[]
}) => {
  const [polarWidgetContainer, setPolarWidgetContainer] =
    useState<BasePolarContainer | null>(null)

  const { mode } = useToggleColorScheme()

  useEffect(() => {
    setPolarWidgetContainer(
      new PolarSettingsContainer({
        track,
        timeStart,
        timeEnd,
        mode,
      }),
    )

    return () => {
      polarWidgetContainer?.destroy()
    }
  }, [mode, track, timeStart, timeEnd])

  return (
    <CanvasContainer
      widget={polarWidgetContainer}
      height={WIDGET_HEIGHT}
      width={WIDGET_WIDTH}
    />
  )
}

export default PolarSettingsWidget
