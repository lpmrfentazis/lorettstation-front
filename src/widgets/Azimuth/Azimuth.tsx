import { Suspense, lazy } from 'react'

const AzimuthWidget = lazy(() => import('./AzimuthWidget'))

export const Azimuth = ({
  azimuth,
  elevation,
}: {
  azimuth: number
  elevation: number
}) => {
  return (
    <Suspense>
      <AzimuthWidget
        azimuth={azimuth}
        elevation={elevation}
      />
    </Suspense>
  )
}
