import * as PIXI from 'pixijs'

import { COLORS, TEXT_OPTIONS } from '@/config/constants'

import { AzimuthContainerProps } from '@/types/control'
import { ThemeMode } from '@/types/main'

export const AZIMUTH_WIDTH = 700
export const AZIMUTH_HEIGHT = 300

const START_COORD = 150
const GAP = 100
const LINE_WIDTH = 5

const RADIUS1 = 100
const RADIUS2 = 150

const COLOR1 = 0xc41c1c
const COLOR2 = 0x0b6bcb

export class AzimuthWidgetContainer extends PIXI.Container {
  private _azimuth = 0
  private _elevation = 0
  private _mode: ThemeMode = 'light'

  private azimuthContainer = new PIXI.Container()
  private azimuthCircle = new PIXI.Graphics()
  private azimuthCursor = new PIXI.Container()
  private azimuthLine = new PIXI.Graphics()
  private azimuthArrow = new PIXI.Graphics()
  private azimuthCurrentLabel = new PIXI.Text('0', TEXT_OPTIONS)
  private azimuthLabels: PIXI.Text[] = []

  private elevationContainer = new PIXI.Container()
  private elevationSemiCircle = new PIXI.Graphics()
  private elevationCursor = new PIXI.Container()
  private elevationLine = new PIXI.Graphics()
  private elevationArrow = new PIXI.Graphics()
  private elevationCurrentLabel = new PIXI.Text('0', TEXT_OPTIONS)
  private elevationLabels: PIXI.Text[] = []

  constructor({ azimuth, elevation, mode }: AzimuthContainerProps) {
    super()

    this.setup()

    this.azimuth = azimuth
    this.elevation = elevation

    this.mode = mode
  }

  get mode() {
    return this._mode
  }

  set mode(mode: ThemeMode) {
    this._mode = mode

    this.azimuthLabels.forEach(item => {
      item.style.fill = COLORS.text[mode]
    })
    this.elevationLabels.forEach(item => {
      item.style.fill = COLORS.text[mode]
    })
  }

  get azimuth() {
    return this._azimuth
  }

  get elevation() {
    return this._elevation
  }

  set azimuth(angle: number) {
    this._azimuth = angle
    this.azimuthCursor.rotation = ((angle - 90) * Math.PI) / 180
    this.azimuthCurrentLabel.text = angle
    this.azimuthCurrentLabel.position.set(
      Math.cos(((angle - 90) * Math.PI) / 180) * (RADIUS1 + 20),
      Math.sin(((angle - 90) * Math.PI) / 180) * (RADIUS1 + 20),
    )
    if (angle % 90 < 10 || angle % 90 > 80) {
      this.azimuthCurrentLabel.alpha = 0
    } else {
      this.azimuthCurrentLabel.alpha = 1
    }
  }

  set elevation(angle: number) {
    this._elevation = angle
    this.elevationCursor.rotation = (angle * Math.PI) / 180 - Math.PI
    this.elevationCurrentLabel.text = angle
    this.elevationCurrentLabel.position.set(
      Math.cos((angle * Math.PI) / 180 - Math.PI) * (RADIUS2 + 20),
      Math.sin((angle * Math.PI) / 180 - Math.PI) * (RADIUS2 + 20),
    )
    if (angle % 90 < 10 || angle % 90 > 80) {
      this.elevationCurrentLabel.alpha = 0
    } else {
      this.elevationCurrentLabel.alpha = 1
    }
  }

  rotateArrows({ azimuth, elevation }: Partial<AzimuthContainerProps>) {
    if (azimuth !== undefined) {
      this.azimuth = azimuth
    }

    if (elevation !== undefined) {
      this.elevation = elevation
    }
  }

  setup() {
    // Создаем графические элементы для азимута
    this.azimuthContainer.position.set(START_COORD, 150)
    this.azimuthCircle.lineStyle(LINE_WIDTH, COLOR1)
    this.azimuthCircle.drawCircle(0, 0, 100)
    this.azimuthContainer.addChild(this.azimuthCircle)

    // Центральная точка азимута
    const azimuthCenterDot = new PIXI.Graphics()
    azimuthCenterDot.beginFill(COLOR1)
    azimuthCenterDot.drawCircle(0, 0, 5)
    azimuthCenterDot.position.set(this.azimuthCircle.x, this.azimuthCircle.y)
    this.azimuthContainer.addChild(azimuthCenterDot)

    // Стрелка азимута
    this.azimuthLine.lineStyle(LINE_WIDTH, COLOR1)
    this.azimuthLine.moveTo(0, 0)
    this.azimuthLine.lineTo(100, 0)
    this.azimuthLine.position.set(0, 0)
    this.azimuthCursor.addChild(this.azimuthLine)

    this.azimuthArrow.lineStyle(LINE_WIDTH, COLOR1)
    this.azimuthArrow.beginFill(COLOR1)
    this.azimuthArrow.moveTo(100, 0)
    this.azimuthArrow.lineTo(80, 10)
    this.azimuthArrow.lineTo(80, -10)
    this.azimuthArrow.closePath()
    this.azimuthArrow.position.set(-4, 0)
    this.azimuthCursor.addChild(this.azimuthArrow)

    // Деления на круге с подписями
    const azimuthMarkers = []
    const labelInterval = Math.PI / 2
    for (let i = 0; i < 4; i++) {
      const angle = i * labelInterval - (90 * Math.PI) / 180
      const labelText = (i * 90).toString()
      const label = new PIXI.Text(labelText, TEXT_OPTIONS)
      label.scale.set(0.5)
      label.anchor.set(0.5)
      label.position.set(
        Math.cos(angle) * (RADIUS1 + 20),
        Math.sin(angle) * (RADIUS1 + 20),
      )

      const marker = new PIXI.Graphics()
      marker.lineStyle(LINE_WIDTH / 2, COLOR1)
      marker.moveTo(-7, 0)
      marker.lineTo(7, 0)
      marker.endFill()
      marker.rotation = i * labelInterval - (90 * Math.PI) / 180

      marker.position.set(Math.cos(angle) * RADIUS1, Math.sin(angle) * RADIUS1)
      this.azimuthContainer.addChild(label)
      this.azimuthContainer.addChild(marker)
      this.azimuthLabels.push(label)
      azimuthMarkers.push(marker)
    }

    this.azimuthContainer.addChild(this.azimuthCursor)
    this.addChild(this.azimuthContainer)

    // Создаем графические элементы для угловой высоты
    this.elevationContainer.position.set(
      START_COORD + RADIUS1 + RADIUS2 + GAP,
      220,
    )

    this.elevationSemiCircle.lineStyle(LINE_WIDTH, COLOR2)
    this.elevationSemiCircle.arc(0, 0, RADIUS2, -Math.PI, 0)
    this.elevationContainer.addChild(this.elevationSemiCircle)

    // Центральная точка угловой высоты
    const elevationCenterDot = new PIXI.Graphics()
    elevationCenterDot.beginFill(COLOR2)
    elevationCenterDot.drawCircle(0, 0, 5)
    elevationCenterDot.position.set(
      this.elevationSemiCircle.x,
      this.elevationSemiCircle.y,
    )
    this.elevationContainer.addChild(elevationCenterDot)

    // Стрелка угловой высоты
    this.elevationLine.lineStyle(LINE_WIDTH, COLOR2)
    this.elevationLine.moveTo(0, 0)
    this.elevationLine.lineTo(150, 0)
    this.elevationLine.position.set(0, 0)

    this.elevationArrow.lineStyle(LINE_WIDTH, COLOR2)
    this.elevationArrow.beginFill(COLOR2)
    this.elevationArrow.moveTo(150, 0)
    this.elevationArrow.lineTo(120, 10)
    this.elevationArrow.lineTo(120, -10)
    this.elevationArrow.closePath()
    this.elevationArrow.position.set(-4, 0)

    // Деления на круге с подписями
    const elevationMarkers = []
    for (let i = 0; i < 3; i++) {
      const angle = i * labelInterval - Math.PI
      const labelText = (i * 90).toString()
      const label = new PIXI.Text(labelText, TEXT_OPTIONS)
      label.scale.set(0.5)
      label.anchor.set(0.5)
      label.position.set(
        Math.cos(angle) * (RADIUS2 + 20),
        Math.sin(angle) * (RADIUS2 + 20),
      )

      const marker = new PIXI.Graphics()
      marker.lineStyle(LINE_WIDTH / 2, COLOR2)
      marker.moveTo(-7, 0)
      marker.lineTo(7, 0)
      marker.endFill()
      marker.rotation = i * labelInterval - Math.PI

      marker.position.set(Math.cos(angle) * RADIUS2, Math.sin(angle) * RADIUS2)
      this.elevationContainer.addChild(label)
      this.elevationContainer.addChild(marker)

      this.elevationLabels.push(label)
      elevationMarkers.push(marker)
    }

    this.elevationCursor.addChild(this.elevationLine)
    this.elevationCursor.addChild(this.elevationArrow)

    this.elevationContainer.addChild(this.elevationCursor)
    this.addChild(this.elevationContainer)

    this.azimuthCurrentLabel.scale.set(0.5)
    this.elevationCurrentLabel.scale.set(0.5)
    this.azimuthCurrentLabel.anchor.set(0.5)
    this.elevationCurrentLabel.anchor.set(0.5)
    this.azimuthContainer.addChild(this.azimuthCurrentLabel)
    this.elevationContainer.addChild(this.elevationCurrentLabel)
  }
}
