import { useEffect, useState } from 'react'

import { useToggleColorScheme } from '@/hooks/colorScheme'

import { AzimuthData } from '@/types/control'

import { CanvasContainer } from '../CanvasContainer/CanvasContainer'
import {
  AZIMUTH_HEIGHT,
  AZIMUTH_WIDTH,
  AzimuthWidgetContainer,
} from './AzimuthContainer'

const AzimuthWidget = ({ azimuth, elevation }: AzimuthData) => {
  const [azimuthWidgetContainer, setAzimuthWidgetContainer] =
    useState<AzimuthWidgetContainer | null>(null)

  const { mode } = useToggleColorScheme()

  useEffect(() => {
    setAzimuthWidgetContainer(
      new AzimuthWidgetContainer({
        azimuth,
        elevation,
        mode,
      }),
    )

    return () => {
      azimuthWidgetContainer?.destroy({ children: true })
    }
  }, [mode])

  useEffect(() => {
    if (!azimuthWidgetContainer) return
    azimuthWidgetContainer.rotateArrows({
      azimuth,
      elevation,
    })
  }, [azimuth, elevation])

  return (
    <CanvasContainer
      widget={azimuthWidgetContainer}
      height={AZIMUTH_HEIGHT}
      width={AZIMUTH_WIDTH}
    />
  )
}

export default AzimuthWidget
